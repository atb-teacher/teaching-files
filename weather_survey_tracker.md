# Weather Survey Tracker

## Goal 1: Track how many times someone says it's sunny
    sunny_counter = 0
    while True:
    	user_input = input("What's the weather? ")
    	if user_input == "sunny":
    		sunny_counter += 1
    	elif user_input == "done":
    		break
    print(sunny_counter)

We have a while loop that runs forever until the user types "done." Honestly, the user doesn't know to type "done," but I want a clean little example focusing on code and not on the user understanding our code.

The loop runs, counting how many times the user says it is sunny, and then breaks when the user types "done."

## Goal 2: Track how many times someone says it's sunny or rainy

	sunny_counter = 0
	rainy_counter = 0 # added line
	 while True:
	 	user_input = input("What's the weather? ")
	 	if user_input == "sunny":
	 		sunny_counter += 1
		elif user_input == "rainy": # added line
			rainy_counter += 1 # added line
	 	elif user_input == "done":
	 		break
	print(sunny_counter) # deleted line
	print(f"It was sunny {sunny_counter} times.") # added line
	print(f"It was rainy {rainy_counter} times.") # added line

Here we have a sunny counter and a rainy counter. We also formatted the output a little bit better at the end.

## Goal 3: Use a dictionary

Let's use a dictionary instead of just variables.

	sunny_counter = 0 # deleted line
	rainy_counter = 0 # deleted line
	weather_mapping = { # added line
		"sunny": 0, # added line
		"rainy": 0, # added line
	} # added line
	 while True:
	 	user_input = input("What's the weather? ")
	 	if user_input == "sunny":
			sunny_counter += 1 # deleted line
			weather_mapping["sunny"] += 1 # added line
	 	elif user_input == "rainy":
			rainy_counter += 1 # deleted line
			weather_mapping["rainy"] += 1 # added line
	 	elif user_input == "done":
	 		break
	print(f"It was sunny {sunny_counter} times.") # deleted line
	print(f"It was rainy {rainy_counter} times.") # deleted line
	print(f"It was sunny {weather_mapping.get('sunny')} times.") # added line
	print(f"It was rainy {weather_mapping.get('rainy')} times.") # added line

Python synax is a little wonky when you are using a dictionary inside of an f-string. It didn't work when I put double-quotes around `sunny` or `rainy`.

The line `weather_mapping["sunny"] += 1` will increment the value associated with the key `"sunny"`. So, if you have a dictionary called `weather_mapping` that looks like `{"sunny": 5}`, and then you run `weather_mapping["sunny"] += 1`, then your dictionary will look like `{"sunny": 6}`.

We don't actually need to check specifically for "sunny" and "rainy" in our if conditionals:

	weather_mapping = {
		"sunny": 0,
		"rainy": 0,
	}
	while True:
		user_input = input("What's the weather? ")
		if user_input == "sunny": # deleted line
			weather_mapping["sunny"] += 1 # deleted line
		elif user_input == "rainy": # deleted line
			weather_mapping["rainy"] += 1 # deleted line
		if user_input in weather_mapping: # added line
			weather_mapping[user_input] += 1 # added line
	 	elif user_input == "done":
	 		break
	print(f"It was sunny {weather_mapping.get('sunny')} times.")

We also don't actually need to have initial values in our dictionary. Here, we will try to check if each key/weather is new, and if it's not, we will increment it. If it is new, we will create a new key with the value `1`, since we've seen that weather exactly one time. Unfortunately, we will get an error.

	weather_mapping = {
		"sunny": 0, # deleted line
		"rainy": 0, # deleted line
	}
	while True:
	 	user_input = input("What's the weather? ")
	 	if user_input in weather_mapping:
	 		weather_mapping[user_input] += 1
		elif user_input not in weather_mapping: # added line
			weather_mapping[user_input] = 1 # added line
	 	elif user_input == "done":
	 		break
	print(f"It was sunny {weather_mapping.get('sunny')} times.")

In the example above, we weren't ever able to get out of the loop. There is no condition where the second `elif` will run, because earlier in the chain we are checking if the string is in or not in the keys of the dictionary. Every string is either in or not in every dictionary.

We should be able to exit our program if we just change where we check for the word "done."

	weather_mapping = {
	}
	while True:
	 	user_input = input("What's the weather? ")
		if user_input in weather_mapping: # deleted line
		if user_input == "done": # added line
			break # added line
		elif user_input in weather_mapping: # added line
	 		weather_mapping[user_input] += 1
	 	elif user_input not in weather_mapping:
	 		weather_mapping[user_input] = 1
		elif user_input == "done": # deleted line
			break # deleted line
	print(f"It was sunny {weather_mapping.get('sunny')} times.")
	print(f"It was rainy {weather_mapping.get('rainy')} times.")

Instead of explicitly saying which weathers we want to look up and print out, we can just loop and print them all out. This is python's equivalent of a `forEach` loop.

	while True:
	 		weather_mapping[user_input] += 1
	 	elif user_input not in weather_mapping:
	 		weather_mapping[user_input] = 1
	print(f"It was sunny {weather_mapping.get('sunny')} times.") # deleted line
	print(f"It was rainy {weather_mapping.get('rainy')} times.") # deleted line
	for weather, count in weather_mapping.items(): # added line
		print(f"It was {weather} {count} times.") # added line

This file is looking good, but the last thing I'd like is a little function to print out `time` or `times` accordingly.

	 weather_mapping = {
	 }
	
	def time_plural(int_param): # added line
		if int_param == 1: # added line
			return f"{int_param} time" # added line
		else: # added line
			return f"{int_param} times" # added line
	
	 while True:
	 	user_input = input("What's the weather? ")
	 	if user_input == "done":
	
And also:

	 	elif user_input not in weather_mapping:
	 		weather_mapping[user_input] = 1
	 for weather, count in weather_mapping.items():
		print(f"It was {weather} {count} times.") # deleted line
		print(f"It was {weather} {time_plural(count)}.") # added line

This is how we can count multiple items.