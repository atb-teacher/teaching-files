# Intro to Flask 2
## A Python framework

For this lab, initialize an empty git repo and run a new git commit after every change. Let me know if you need a git refresher. You can also review the git guide I made [here](git.md).

Go to `replit` and start a new project. There should be a `flask` option. It will start you with a file that looks like this:

```python
from flask import Flask

app = Flask(__name__)

@app.route('/')
def index():
    return 'Hello from Flask!'

app.run(host='0.0.0.0', port=81)
```

You can run this program, and it will start a Flask server, and that server will host a webpage that says "Hello from Flask!"

First, lets add more html to the return value.

```python

 @app.route('/')
 def index():
    return 'Hello from Flask!' # deleted line
    return """ # added line
    <h1>Hello from Flask!</h1> # added line
    <form method="post" action="/"> # added line
    <p> # added line
    <label for="first-name">What's your first name?</label> # added line
    <input type="text" name="first-name"> # added line
    </p> # added line
    <p> # added line
    <label for="food">What's your fav food?</label> # added line
    <input type="text" name="food"> # added line
    <input type="submit"> # added line
    </p> # added line
    </form> # added line
    """ # added line

```

Here's the string without all the `# added line` comments:

```python
    return """
    <h1>Hello from Flask!</h1>
    <form method="post" action="/">
    <p>
    <label for="first-name">What's your first name?</label>
    <input type="text" name="first-name">
    </p>
    <p>
    <label for="food">What's your fav food?</label>
    <input type="text" name="food">
    <input type="submit">
    </p>
    </form>
    """ # added line

```

This gives us an html form. The form will send info via an html request, specifically using the `post` method. There are four main methods of html requests: POST, GET, PUT, and DELETE. These correspond to the methods in `CRUD`: CREATE, RETRIEVE, UPDATE, DELETE. In reality people use `get` and `post` for almost everything -- these methods have changed from their initial purposes. Generally, `get` means "just look at the url", and `post` means "look at the body."

We can access the info submitted to this form by editing the code here:

```python

from flask import Flask # deleted line
from flask import Flask, request # added line
 
 app = Flask(__name__)
 
 
@app.route('/') # deleted line
@app.route('/', methods=["GET", "POST"]) # added line
 def index():
    if request.method == "POST": # added line
       print(request.form["first-name"]) # added line
       print(request.form["food"]) # added line
    return """
    <h1>Hello from Flask!</h1>
    <form method="post" action="/">

```

Now, when you enter info and hit "submit", that info should be printed out in the console.

Whenever you see a `@` symbol in python, that means that you're looking at a decorator. You can look at this example decorator code below. You don't have to code it or understand it, really.

```python
def add_q(in_fun):
    def out_fun(in_string):
        return in_fun(in_string) + "?????"
    return out_fun

@add_q
def add_e(in_string):
    return in_string + "!!!!"

print(add_e("hello"))
```

Running that code will print `hello!!!!?????`.

Now we will add some database stuff.

We will create a class called `User`, and Flask will create a corresponding database table called `User.` We will choose the columns in this database.

```python

from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy # added line
 
db = SQLAlchemy() # added line
app = Flask(__name__)
 
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///project.db" # added line

db.init_app(app) # added line

class User(db.Model): # added line
    id = db.Column(db.Integer, primary_key=True) # added line
    firstname = db.Column(db.String, nullable=False) # added line
    food = db.Column(db.String) # added line

with app.app_context(): # added line
    db.create_all() # added line
```

Now we can save data into our database. We won't see the consequences immediately.

```python
def index():
     if request.method == "POST":
       print(request.form["first-name"])
       print(request.form["food"])
       user = User( # added line
         firstname=request.form["first-name"], # added line
         food=request.form["food"], # added line
       ) # added line
       db.session.add(user) # added line
       db.session.commit() # added line
     return """
     <h1>Hello from Flask!</h1>
     <form method="post" action="/">

```

To use the info in our database and have it affect our template, first we need to import the `render_template` method

```python

from flask import Flask, request # deleted line
from flask import Flask, request, render_template # added line
from flask_sqlalchemy import SQLAlchemy

```

Then we need to delete a bunch of html code. We will also query the database for all Users, and provide that info to the `render_template` method as it creates an http response.

```python

       db.session.add(user)
       db.session.commit()
    return """ # deleted line
    <h1>Hello from Flask!</h1> # deleted line
    <form method="post" action="/"> # deleted line
    <p> # deleted line
    <label for="first-name">What's your first name?</label> # deleted line
    <input type="text" name="first-name"> # deleted line
    </p> # deleted line
    <p> # deleted line
    <label for="food">What's your fav food?</label> # deleted line
    <input type="text" name="food"> # deleted line
    <input type="submit"> # deleted line
    </p> # deleted line
    </form> # deleted line
    """ # deleted line

 # added line
    users = User.query.all() # added line
       # added line
    return render_template("index.html", users=users) # added line

```

Now, Flask will be confused because it tries to find a file called `index.html` in the correct location. You can create a folder called `templates`, and inside that folder put [this file](index.html).

If you look in that html file, you'll see some parts that looks like this: `{{ stuff }}` or `{% stuff %}`. That's the template language. That is used to allow the info you pass into `render_template` actually affect the http response.

After you make that file, you can put info in both text boxes and hit "submit." The data will be saved to the database. Every time the page is rendered, the database will be accessed and flask will combine the database query with the template to create an http response. If you want to browse the database, you can download it from replit and access it with the program `sqlitebrowser`, available [here](https://sqlitebrowser.org/).
