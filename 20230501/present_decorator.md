# Present Decorator
## A Java Lab
## 5 points

Make a file using the decorator pattern. You will need an interface called `Present` that looks like this:

```java
interface Present {
        public void Unwrap();
}
```

You will also need a class `PresentBox` and a class `WrappingPaper` that implements `Present`.

`PresentBox` should be the core of the present. `WrappingPaper` should take in a `Present` as a parameter in the constructor method, as well as a color. Running the `Unwrap` method should unwrap the whole present.

The main method should look something like this:

```java
public static void main(String[] args) {
        Present myPresent = new PresentBox();
        myPresent = new WrappingPaper("blue", myPresent);
        myPresent = new WrappingPaper("green", myPresent);
        myPresent = new WrappingPaper("red", myPresent);
        myPresent.Unwrap();
}
```

and it should print out something like this:

```
Unwrapping the red paper...
Unwrapping the green paper...
Unwrapping the blue paper...
You got to the box!
```

[Hint file -- MechSuit](MechSuit.java)
