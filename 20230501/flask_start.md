# Intro to Flask
## A Python framework

For this lab, initialize an empty git repo and run a new git commit after every change. Let me know if you need a git refresher. You can also review the git guide I made [here](git.md).

Go to `replit` and start a new project. There should be a `flask` option. It will start you with a file that looks like this:

```python
from flask import Flask

app = Flask(__name__)

@app.route('/')
def index():
    return 'Hello from Flask!'

app.run(host='0.0.0.0', port=81)
```

You can run this program, and it will start a Flask server, and that server will host a webpage that says "Hello from Flask!"

If we change the return value of this function, you can see the webpage change, too.

```python
 @app.route('/')
 def index():
    return 'Hello from Flask!' # deleted line
    return 'Hello you!' # added line
```

If you make this change, it should say "Hello, you!" instead of "Hello from Flask!" Now let's make a second function, which will allow a parameter in the URL. Remember, in python if you see three quotes, `"""`, that begins or ends a large multi-line string. Theoretically, you could probably put three quotes and then paste an entire html webpage and three more quotes, and return an entire webpage as a string. We're just going to return a little text and a little html.

In python, a string that starts with `f` means that it allows for interpolation, similar to how String.format in java allows for interpolation.

We are using the `escape` function to make sure no one puts in any malicious code.

```python
from flask import Flask
from markupsafe import escape # added line
 
 app = Flask(__name__)
 
 @app.route('/')
 def index():
    return 'Hello you!' # deleted line
    return """Hello you! # added line
    <a href="/your-name-here">click me!</a> # added line
    """ # added line
   # added line
@app.route('/<name>') # added line
def index2(name): # added line
    return f'Hello {escape(name)}!' # added line
 
 app.run(host='0.0.0.0', port=81)

```

Now, a link should appear to a page that says, "Hi, your-name-here!" You can change the html, and it will change the resulting page.

We can make the link stop working by adding to the URL path associated:

```python
   
@app.route('/<name>') # deleted line
@app.route('/custom-hi/<name>') # added line
 def index2(name):
     return f'Hello {escape(name)}!'
```

And we can make the link work again by editing the link itself:

```python
@app.route('/')
def index():
    return """Hello you!
    <a href="/your-name-here">click me!</a> # deleted line
    <a href="/custom-hi/your-name-here">click me!</a> # added line
    """
```

We don't want to throw html into this main python file forever, so let's create a folder called `templates` and start using html files from that folder. The first file we will create will be called `index.html`. It'll just have the html that we were returning in the function previously. For reference, you can see the templates [here](templates/).

```python
from flask import Flask # deleted line
from flask import Flask, render_template # added line
 from markupsafe import escape
 
 app = Flask(__name__)
 
 @app.route('/')
 def index():
    return """Hello you! # deleted line
    <a href="/custom-hi/your-name-here">click me!</a> # deleted line
    """ # deleted line
    return render_template("index.html") # added line

```

We can also have interpolation in our html templates using the template language provided by flask (flask isn't bound to a single template language, but it uses Jinja by default). Add the template [name.html](templates/name.html) to the templates folder.

```python
@app.route('/custom-hi/<name>')
def index2(name):
    return f'Hello {escape(name)}!' # deleted line
    return render_template("name.html", template_name=name) # added line
 
app.run(host='0.0.0.0', port=81)
```
