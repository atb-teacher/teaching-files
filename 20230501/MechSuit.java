import java.util.Scanner;

interface Body {
	public void Punch();
	public void Jump();
}

class Human implements Body {
	public Human() {
	}
	public void Punch() {
		System.out.println("The human uses its weak muscles to punch");
	}
	public void Jump() {
		System.out.println("The human uses its weak muscles to jump");
	}
}


class MechSuit implements Body {
	Body pilot;
	public MechSuit(Body pilotParam) {
		pilot = pilotParam;
	}
	public void Punch() {
		pilot.Punch();
		System.out.println("The mech suit punches with powerful force!");
	}
	public void Jump() {
		pilot.Jump();
		System.out.println("The mech suit jumps with powerful force!");
	}

	public static void main(String[] args) {
		Scanner myScan = new Scanner(System.in);
		Body pilot = new Human();
		System.out.println("Does the pilot put on a mech suit?(y/N)");
		String response = myScan.nextLine();
		if ( response.toLowerCase().equals("y") ) {
			pilot = new MechSuit(pilot);
		}
		pilot.Punch();
		pilot.Jump();
	}
}
