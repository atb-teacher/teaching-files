# Unit Testing 1

This lab will use multiple terms from pages 11 and 12 of the textbook, including: `unit testing`, `normal data`, `data at the limits`, `extreme data`, and `abnormal data`.

Choose two labs that we've done recently, out of `Reverse Polish Notation Lab`, `Ordering System (from UML Diagram)`, and `Cached Fibonacci`. Write tests for a class or multiple classes in that lab.

You must write a minimum of three tests per lab (for example, you could run `assertEquals` three times). Overall (not per lab), you must have at least one example of each type of test: `normal data` (the easiest), `data at the limits`, `extreme data`, and `abnormal data`.

When you write a test, add a comment to clarify which type of test it is. For example:

```java
assertEquals(Adder.add(2, 3), 5); // normal
```

[more info here](abc.html)