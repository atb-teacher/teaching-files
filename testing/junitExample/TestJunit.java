import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/*
 * javac -cp "/usr/share/java/junit4.jar:." TestJunit.java
 * java -cp "/usr/share/java/junit4.jar:." TestJunit
 */

public class TestJunit {
	public static void main(String[] args) {
		testAdd();
		// testAddDoomed();
	}
   public static void testAdd() {
      assertEquals(Adder.add(2, 3), 5);
      assertTrue(true);
      // assertTrue(false);
      lunchOrder l1 = new lunchOrder(2, "SANDWICH");
      lunchOrder l2 = new lunchOrder(2, "SANDWICH");
      System.out.println(l1.hashCode());
      System.out.println(l2.hashCode());
      assertTrue( 
		new lunchOrder(2, "SANDWICH").checkEquals(
			new lunchOrder(2, "SANDWICH")
		)
	);
   }
   public static void testAddDoomed() {
      assertEquals(Adder.add(2, 3), 6);
   }
}

class lunchOrder {
	int amount;
	public enum Food {
		SANDWICH,
		BURRITO,
		RICE_BOWL,
	}
	Food food;

	lunchOrder(int foodAmount, String inFood) {
		this.amount = foodAmount;
		this.food = Food.valueOf(inFood);
	}

	public boolean checkEquals(lunchOrder lOther) {
		System.out.println((this.amount == lOther.amount) && (this.food == lOther.food));
		return (this.amount == lOther.amount) && (this.food == lOther.food);
	}

}
