public class KludgeTester {
	public static void assertEquals(int num1, int num2) {
		if (num1 != num2) {
			System.out.println("Fail!");
		} else System.out.println("Pass!");
	}
	public static void assertEquals(String str1, String str2) {
		if (str1 != str2) {
			System.out.println("Fail!");
		} else System.out.println("Pass!");

	}
	public static void assertTrue(boolean inBool) {
		if (!inBool) {
			System.out.println("Fail!");
		} else System.out.println("Pass!");
	}
	public static void fail() {
		System.out.println("Fail!");
	}
}
