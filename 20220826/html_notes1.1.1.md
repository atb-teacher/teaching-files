# HTML Notes part 1

Take notes on a section of the book, in an HTML file. Your notes should combine to be a minimum of 200 words, or 150 words if you have use a list element. Word count should not include html code.

Turn in a file with the given filename, or a link to a replit that has the given filename as a project name. In replit, we'll keep the main file named `index.html`. You should not work with other students who have your section (that will come later).

Here are the sections of the book you should take notes on:


```
Part 1
1.1.1, 1.1.2
filename: 1.1.1-2.html
```

* TJ, Tanush, Kieran

-----

```
Part 2
1.1.3, 1.1.4
filename: 1.1.3-4.html
```

* Sofronis, Kyle, Reid 
-----

```
Part 3
1.1.5, 1.1.6
filename: 1.1.5-6.html
```

* Carter, Braden, Lucas
-----


```
Part 4
1.1.7
filename: 1.1.7.html
```

* Declan, Nicholas, Aila
-----


```
Part 5
1.1.8, 1.1.9
filename: 1.1.8-9.html
```

* Matthew, Siena, Nathaniel
-----


```
Part 6
1.1.10, 1.1.11
filename: 1.1.10-11.html

```
* Jacob, Farzaan, Isaac
-----


```
Part 7
1.1.12, 1.1.13, 1.1.14
filename: 1.1.12-14.html
```

* Thalen, Dillon, Ethan
-----

