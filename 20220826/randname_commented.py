import argparse
import json
from random import randrange as rg

# ========== Setup Argument Parser ==========

parser = argparse.ArgumentParser(description="take in a file")
parser.add_argument('file', type=str, nargs=1)


args = parser.parse_args() # grab argument (probably "example.json")

# ========== open example.json and get info ==========

with open(args.file[0], "r") as f:
    output = f.read()
print(output)

# ========== convert info into a python list ==========

namelist = json.loads(output)['names']


while True: # forever loop
    active_names = namelist.copy() # make a copy of the list of names
    while active_names: # while the list of names isn't empty
        print(active_names.pop(rg(len(active_names)))) # remove a random name and print it out
        print(active_names) # print out remaining names
        input() # wait for the user to push enter
