# Sorting Algorithm #1 - BogoSort

To begin looking at sorting algorithms, we will start with a very inefficient algorithm: Bogo Sort

## Goal 1: Randomly order a list of two items, and check if the list is sorted

	import random # added line

	my_list = [2, 1] # added line
	counter = 0 # added line
	while True: # added line
		counter += 1 # added line
		random.shuffle(my_list) # added line
		if my_list[0] < my_list[1]: # added line
			print(f"Completed in {counter} tries") # added line
			break # added line

We start with an out-of-order list, `[2, 1]`. Then we have a loop that goes forever until we break it. Inside the loop, we shuffle the list, and we check if the first item is lower than the second item. When that is true, we break the loop.

<h4>Make a git commit here</h4>

Whenever you make a git commit, make sure you have a meaningful message with `git commit -m "meaningful message"

<hr />

## Goal 2: Randomly order a list of three items, and check if the list is sorted

	import random
	 
	my_list = [2, 1] # deleted line
	my_list = [2, 3, 1] # added line
	counter = 0
	while True:
		counter += 1
	 	random.shuffle(my_list)
		if my_list[0] < my_list[1]: # deleted line
		if my_list[0] < my_list[1] < my_list[2]: # added line
	 		print(f"Completed in {counter} tries")
	 		break

<h4>Make a git commit here</h4>
<hr />

Here, we do the same thing, only with three places.

## Goal 3: Come up with a better way to check if our list is sorted

So far we are always looking at the indecies 0, 1, and 2. Those numbers are hardcoded in. Ideally, these numbers should change based on the length of our list.

For this, we are going to create an entirely new function above our code, so that we can test it out separately and then use it in our code.

We won't be using our old code for a bit, so you can comment it out. In python, it's easy to comment out code by turning it into a large string, by adding `"""` before and after the code.

	import random
	
    """  # added line
	my_list = [2, 1]
	my_list = [2, 3, 1]
	counter = 0
	while True:
		counter += 1
	 	random.shuffle(my_list)
		if my_list[0] < my_list[1]:
		if my_list[0] < my_list[1] < my_list[2]:
	 		print(f"Completed in {counter} tries")
	 		break
    """  # added line

<h4>Make a git commit here</h4>
<hr />

I couldn't think of what to do immediately, so I just made this function:

	def check_sorted(in_list): # added line
		found_unordered_pair = False # added line
		for item in in_list: # added line
			print(item) # added line
	
	my_list = [2, 3, 1]
	
	check_sorted(my_list) # added line

<h4>Make a git commit here</h4>
<hr />

What's nice about this is that we can see it go through the list and print out every element. It prints out the `2`, and then the `3`, and then the `1`. 

To check if the list is sorted, you can imagine every number looking to the number on the right, and then seeing if it is bigger. The `2` would look right, and find the `3`, and so far the list would look sorted, because `2` is less than `3`. Then the `3` would look to the right and see the `1`, and it would not look sorted because `3` is more than `1`. If instead of our list being `[2, 3, 1]` it were `[2, 3, 4]`, our list would be sorted, and when both the `2` and the `3` looked to the right they would see bigger numbers.

First let's look at a convoluted way to do this. For each of our three numbers, 2, 3, and 1, we can use `list.index()` to get the index that that number resides at. We can add that number by 1, and then we will have the index of the number on the right.

    >>> my_list = [2, 3, 1]
    >>> active_index = my_list.index(2)
    >>> print(active_index)
    0
    >>> right_index = active_index + 1
    >>> print(right_index)
    1
    >>> my_list[right_index]
    3
    >>> 2 < my_list[right_index]
    True

So, if we have the number `2` as an item in the list, we can use this method to get the item to the right of the number `2` in the list.

Of course, it's always more difficult to go from an item in a list/array to an index. The easiest thing is to keep track of the indecies, and to base everything around those.

    >>> my_list = [2, 3, 1]
    >>> len(my_list)
    3
    >>> for i in range(len(my_list)):
    ...     print(i)
    ... 
    0
    1
    2
    >>> for i in range(len(my_list)):
    ...     print(my_list[i])
    ... 
    2
    3
    1
    >>> for i in range(len(my_list)):
    ...     print(my_list[i] < my_list[i + 1])
    ... 
    True
    False
    Traceback (most recent call last):
      File "<stdin>", line 2, in <module>
    IndexError: list index out of range

Focusing on the indecies works well here, until it doesn't. For some reason, it says we have an `IndexError`. The problem here is that we keep on trying to look to the right, and we do it one too many times. If the `2` looks to the right, it will find the `3`, and if the `3` looks to the right, it will find the `1`. But what if we tell the `1` to look to the right? Then it won't find anything, and python will get upset that we tried to look there. So, we will have to remember that we do not want to look right of the maximum index. Sorry, but I'm going to make you see this error again.

	import random
	 
	def check_sorted(in_list):
		found_unordered_pair = False
		for item in in_list: # deleted line
			print(item) # deleted line
		for index in range(len(in_list)): # added line
			if in_list[index] > in_list[index + 1]: # added line
				found_unordered_pair = True # added line
		return not found_unordered_pair # added line
	 
	my_list = [2, 3, 1]
	 
	check_sorted(my_list) # deleted line
	print(check_sorted(my_list)) # added line

<h4>Make a git commit here</h4>
<hr />

Let's try to look at what's going on when we get the error:

So it looks like the list's length is 3, and we are trying to look at index `2`, as well as to the right of index `2`, and that number is too high. We should remember that, generally when you know the length of a list, the numbers you can use as indecies are 0 through length - 1. Python's `range` is smart, so if you give it a number, it will give you `0` through that number - 1, assuming that you will want every usable index. However, if we want to safely look to the right, we need to make sure that we aren't looking at the last index, so we need to look at the numbers 0 through length - 2.

	def check_sorted(in_list):
	 	found_unordered_pair = False
        list_length = len(in_list) # added line
		reduced_list_length = list_length - 1 # added line
		for index in range(reduced_list_length): # added line
			if in_list[index] > in_list[index + 1]: # deleted line
				found_unordered_pair = True # deleted line
			try: # added line
			    if in_list[index] > in_list[index + 1]: # added line
			    	found_unordered_pair = True # added line
			except IndexError: # added line
				print("Error!") # added line
				print(f"The list's length is {len(in_list)}") # added line
				print(f"The index we're trying to look at is {index}") # added line
	 	return not found_unordered_pair

<h4>Make a git commit here</h4>
<hr />

You don't see `- 2` anywhere in this code: you see `- 1` to get `reduced_list_length`, and `range` naturally subtracts one off the number you put in. Here is a bit of similar sample code that you don't have to type along with, it's just in case you aren't too familiar with how `range` works:

    import random
    
    def check_sorted(in_list):
    	found_unordered_pair = False
    	list_length = len(in_list)
    	reduced_list_length = list_length - 2
    	index = 0
    	while index <= reduced_list_length:
    		if in_list[index] > in_list[index + 1]:
    			found_unordered_pair = True
    		index += 1
    	return not found_unordered_pair

This example should work, just like the previous example did.

Now we can clean up our `check_sorted` function a bit.

	 	list_length = len(in_list)
	 	reduced_list_length = list_length - 1
	 	for index in range(reduced_list_length):
			try: # deleted line
			    if in_list[index] > in_list[index + 1]: # deleted line
			    	found_unordered_pair = True # deleted line
			except IndexError: # deleted line
				print("Error!") # deleted line
				print(f"The list's length is {len(in_list)}") # deleted line
				print(f"The index we're trying to look at is {index}") # deleted line
			if in_list[index] > in_list[index + 1]: # added line
				found_unordered_pair = True # added line
	 	return not found_unordered_pair

<h4>Make a git commit here</h4>
<hr />

One problem with our function is that, if it finds a pair of items that are out of order, it will continue to look at the entire list. To fix this, we can look at the line `found_unordered_pair = True`, and we can try to figure out how to stop our function if that line of code runs. It could be done with a `break` statement, but we're just going to stop the function with a `return` statement.

	 	reduced_list_length = list_length - 1
	 	for index in range(reduced_list_length):
	 		if in_list[index] > in_list[index + 1]:
				found_unordered_pair = True # deleted line
				return True # added line
	 	return not found_unordered_pair

<h4>Make a git commit here</h4>
<hr />

Of course, now if we look at the variable `found_unordered_pair`, it is set to `False` and then never changed, so we don't really need it.


	def check_sorted(in_list):
		found_unordered_pair = False # deleted line
	 	list_length = len(in_list)
	 	reduced_list_length = list_length - 1
	 	for index in range(reduced_list_length):
	 		if in_list[index] > in_list[index + 1]:
	 			return True
		return not found_unordered_pair # deleted line
		return False # added line

<h4>Make a git commit here</h4>
<hr />

We can also remove the variables `list_length` and `reduced_list_length`.

	 def check_sorted(in_list):
		list_length = len(in_list) # deleted line
		reduced_list_length = list_length - 1 # deleted line
		for index in range(reduced_list_length): # deleted line
		for index in range(len(in_list) - 1): # added line
	 		if in_list[index] > in_list[index + 1]:
	 			return True
	 	return False

<h4>Make a git commit here</h4>
<hr />

Now our bogosort algorithm can use our `check_sorted` function, and we don't have to worry about how long the list is. We should also remember to remove the `"""`s we used to comment out the Bogo Sort code.

	def check_sorted(in_list):
	 	for index in range(len(in_list) - 1):
	 		if in_list[index] > in_list[index + 1]:
				return False
		return True
	 
	my_list = [2, 3, 1]
	 
	counter = 0
	while True:
	 	counter += 1
	 	random.shuffle(my_list)
		if my_list[0] < my_list[1] < my_list[2]: # deleted line
		if check_sorted(my_list): # added line
			print(my_list) # added line
	 		print(f"Completed in {counter} tries")
	 		break

<h4>Make a git commit here</h4>
<hr />

To better organize this file, we should put our algorithm in a function. (This section says delete/add a lot, but mostly you just have to indent stuff).

	def check_sorted(in_list):
	 	for index in range(len(in_list) - 1):
	 		if in_list[index] > in_list[index + 1]:
	 			return False
	 	return True
	 
	def bogosort(in_list): # added line
		counter = 0 # added line
		while True: # added line
			counter += 1 # added line
			random.shuffle(in_list) # added line
			if check_sorted(in_list): # added line
				print(in_list) # added line
				print(f"Completed in {counter} tries") # added line
				return in_list # added line
	 
	counter = 0 # deleted line
	while True: # deleted line
		counter += 1 # deleted line
		random.shuffle(my_list) # deleted line
		if check_sorted(my_list): # deleted line
			print(my_list) # deleted line
			print(f"Completed in {counter} tries") # deleted line
			break # deleted line

	my_list = [2, 3, 1]
	bogosort(my_list) # added line

And we can make it easier to experiment with list lengths.

	my_list = [2, 3, 1] # deleted line
	my_list = list(range(5)) # added line
	random.shuffle(my_list) # added line
	bogosort(my_list)

<h4>Make a git commit here</h4>
<hr />
