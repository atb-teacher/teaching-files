# Lab: Password Generator

## A Java Lab

Let's create a password generator, which should both print and return a password. Allow the method that creates the password to take in an integer that determines the length, or let it default to a length of 10 if not given a number.

You can use this string:

    abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~

Here's a bit of python code as a hint on how to build a string in a loop.

    >>> my_str = ''
    >>> counter = 0
    >>> while counter < 5:
    ...     my_str = my_str + 'a'
    
    ...     counter = counter + 1
    ... 
    >>> print(my_str)
    aaaaa

## Bonus Goal (+1 extra credit point)

Allow the method to take in the number of numbers, the number of letters, and the number of punctuation marks, and make a password that combines all of those.
