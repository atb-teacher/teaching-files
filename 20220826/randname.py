import argparse
import json
from random import randrange as rg
parser = argparse.ArgumentParser(description="take in a file")
parser.add_argument('file', type=str, nargs=1)
args = parser.parse_args()
with open(args.file[0], "r") as f:
    output = f.read()
print(output)
namelist = json.loads(output)['names']
while True:
    active_names = namelist.copy()
    while active_names:
        print(active_names.pop(rg(len(active_names))))
        print(active_names)
        input()
