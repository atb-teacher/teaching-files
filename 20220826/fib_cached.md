# Cached Fibonacci Guide
## Python

We will start by making a class that produces the numbers of the [Fibonacci Sequence](https://simple.wikipedia.org/wiki/Fibonacci_number).

Let's start by just having a python class that simply knows the first few Fib numbers (this is kinda cheating, we have a tuple of correct answers and we look it up).

```python
def main():
    my_fib = Fib()
    print(my_fib.fib(4))

class Fib:
    beginning_nums = (0, 1, 1, 2, 3, 5, 8)
    def fib(self, in_num):
        return self.beginning_nums[in_num]

main()
```

<h4>Make a git commit here</h4>

Whenever you make a git commit, make sure you have a meaningful message with `git commit -m "meaningful message"

<hr />

Now let's actually add some level of recursion, so that we can calculate any number.

```python
def main():
    my_fib = Fib()
    print(my_fib.fib(4)) # deleted line
    print(my_fib.fib(10)) # added line
 
class Fib:
    beginning_nums = (0, 1, 1, 2, 3, 5, 8) # deleted line
    beginning_nums = (0, 1, 1) # added line
    def fib(self, in_num):
        return self.beginning_nums[in_num] # deleted line
        if in_num < 3: # added line
            return self.beginning_nums[in_num] # added line
        return self.fib(in_num - 1) + self.fib(in_num - 2) # added line
``` 

<h4>Make a git commit here</h4>
<hr />

Python is pretty slow to do math like this, so it should be sluggish around 30 - 40 I think? There's a lot of recursion that happens to calculate a Fibonacci number, so it can take a while.

Instead of using our `Fib` class, we can create a new class, `FibCached`, which inherits from our `Fib` class.

Instead of calculating each Fibonacci number, it would be easier to just create a `dictionary`/`HashMap` to store any numbers.

```python
def main():
    my_fib = Fib() # deleted line
    my_fib = FibCached() # added line
    print(my_fib.fib(10))
```

and

```python
class FibCached(Fib): # added line
    def __init__(self): # added line
        self.cache = {} # added line
    def fib(self, in_num): # added line
        if in_num not in self.cache: # added line
            self.cache[in_num] = super().fib(in_num) # added line
        return self.cache[in_num] # added line
```

<h4>Make a git commit here</h4>
<hr />

This should go way faster for numbers that were sluggish before.

Now let's let the user choose whether or not they want the cached version.

```python
def main():
    my_fib = FibCached() # deleted line
    if input("cached? (y/n) ") == "y": # added line
        my_fib = FibCached() # added line
    else: # added line
        my_fib = Fib() # added line
    print(my_fib.fib(10))
```

<h4>Make a git commit here</h4>
<hr />
