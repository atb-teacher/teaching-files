# Anagram and Palindrome lab
## A Java Lab

Create a program that can tell the user if they have entered one word that is a palendrome, or two words that are anagrams. The program should have a  REPL (read evaluate print loop, a while loop that lets the user add info) that lets the user choose `anagram` and enter two words, or lets the user choose `palindrome` and enter one word. The program should ignore commas and spaces.

Using the program should look something like this:

```
Would you like to check (a)nagram or (p)alindrome, or (q)uit? a
First word: car
Second word: rac
Those two words are anagrams!
Would you like to check (a)nagram or (p)alindrome, or (q)uit? p
Word: racerac
That word is not a palindrome!
Would you like to check (a)nagram or (p)alindrome, or (q)uit? q
Bye!
```

Hint file [here](./SameLetters.java).
