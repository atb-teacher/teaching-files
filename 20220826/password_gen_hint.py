import random

LOOP_NUM = 10

PASSWORD_LIST = list("abc")

output = ""

for i in range(LOOP_NUM):
    list_length = len(PASSWORD_LIST) # list_length = 3
    random_index = random.randint(0, list_length - 1) # random number btw 0 and 2
    output = output + PASSWORD_LIST[random_index] # random letter btw a, b, c

print(output)

"""
You'll want to make sure that you are returning the password in addition to printing it out
You'll want to figure out how to get a random index of an array or arraylist in java
"""
