# Using Git on Replit.com

We will be running Unix/Linux shell commands in the replit shell.

![shell](replit_shell.png)

In this guide we will learn the commands:
  * `pwd`
  * `mkdir`
  * `cd`
    * `cd ..`
  * `ls`
    * `ls -a`

as well as the git commands:
  * `init`
  * `add`
    * `add .`
    * `add file.py`
  * `commit`
    * `commit -m "short message"`
    * `commit -m "short message" -m "longer message"`
  * `whatchanged -p`
  * `touch`

## Getting to the right folder

First, we need to make sure that we are in the corrcet folder. You can check where you are with the "print working directory" command. Just type `pwd` into the shell.

```
pwd
```

Why don't we make a directory to store our files, as well as our git repo. We can make a directory called "my\_dir" by running `mkdir my_dir`.


```
mkdir my_dir
```

We can use `pwd` again to see that we are not inside the directory. To enter the directory (similar to double-clicking the folder image in Windows) we can type `cd my_dir`. `cd` is short for "change directory."

```
cd my_dir
```

You can see where you are now with `pwd`. If you ever want to take a step out of a directory, you can run `cd ..`.

Now we can initialize a git repo. I like to think about this as creating a little bundle of history. To do this you can run `git init` while inside your `my_dir` directory.

```
git init
```

We can make a file with `touch`.

```
touch my_file.txt
```

Now git needs a name and email to associate us with.

```
~/bogo-1$ git config --global user.name "whatever"
~/bogo-1$ git config --global user.email "fake@fake.com"
```

Now you can use replit's text editor to add some text into the file. Add something wacky.

We can tell git to observe our file with `git add my_file.txt`, or we can tell git to observe all changes to the folder with `git add .`. 

Now we can tell git to create a node of history with `git commit -m "first change" -m "I'm trying out git for the first time"`.

Now change `my_file.txt` again. Once again, run `git add my_file.txt`. Run `git commit -m "added more text"`.

Now, we can see all the changes with `git whatchanged -p`. It will default to showing all the changes in a program called `less`. You can move up in `less` by pushing `ctrl`+`u`, you can move down with `ctrl`+`d`, and you can quit by pushing `q`.


