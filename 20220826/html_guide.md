
# HTML Guide

Let's start with a head and a body:

```html
<head>
</head>
<body>
	<div>
		Hello there!
	</div>
</body>
```

The important points here: everything is inside of `<html></html>`. Everything inside of `<head></head>` will not be seen as part of the main document. Everything inside of `<body></body>` will be seen as part of the main document.

Let's put stuff in the `head`.

```html
<head>
	<title>My HTML Page!</title>    <!-- added -->
</head>
<body>
	<div>
```

The main document looks exactly the same. The only change we should see is the text in the tab, if we open this html page in a browser.

Now we can add `<style></style>` tags with some CSS inside. You will notice that the syntax is slightly different between the `<style></style>` tags.

```html
<head>
	<title>My HTML Page!</title>
<style>    /* added */
div {    /* added */
	background-color: skyblue;    /* added */
}    /* added */
</style>    <!-- added -->
</head>
<body>
	<div>
```

Between the `style` tags we can put some css. The css helps you determine how everything will look. There's a common analogy for websites -- the `html` is the skeleton, the `css` is the feathers`, and the `javascript` is the muscle.

Here we say `div {}` which means "Look for a div with the id of 'bluediv' and apply these styling rules to that div." Inside that div we decided to apply the rule that the background color will be sky blue.

This rule should apply to multiple divs as long as we have multiple divs.


```html
	<div>
		Hello there!
	</div>
	<div>    <!-- added -->
		2nd div    <!-- added -->
	</div>    <!-- added -->
</body>
```

Because we have multiple divs, the `div` selector in the `style` tags will find every `div` and apply the same styling to them. We can specify html elements using and ID.


```html
<head>
	<title>My HTML Page!</title>
<style>
div {    /* deleted */
div#bluediv {    /* added */
	background-color: skyblue;
}
</style>
</head>
<body>
	<div>    <!-- deleted -->
	<div id="bluediv">    <!-- added -->
		Hello there!
	</div>
	<div>
```

Between the `style` tags we can put some css. The css helps you determine how everything will look. There's a common analogy for websites -- the `html` is the skeleton, the `css` is the feathers`, and the `javascript` is the muscle.

Here we say `div#bluediv`. In css, a number sign `#` means "look for something with this ID, whereas a period `.` means "look for something with this class. In this case we typed `div#bluediv`, which means "Look for a div with the id of 'bluediv' and apply these styling rules to that div."

Now we can create a class, which is similar to an id, but it allows us to create rules for multiple elements.


```html
div#bluediv {
	background-color: skyblue;
}
div.rightjust {    <!-- added -->
	text-align: right;    <!-- added -->
}    <!-- added -->
</style>
</head>
<body>
	<div id="bluediv">    <!-- deleted -->
	<div id="bluediv" class="rightjust">    <!-- added -->
		Hello there!
	</div>
	<div>    <!-- deleted -->
	<div class="rightjust">    <!-- added -->
		2nd div
	</div>
</body>
```

`div.rightjust` will apply to every div that includes `rightjust` in its classes. You can see now that both divs are text-aligned to the right.

If we add another type of element that's not a div, that class will not apply.


```html
	<div class="rightjust">
		2nd div
	</div>
	<p class="rightjust">    <!-- added -->
		This is a paragraph    <!-- added -->
	</p>    <!-- added -->
</body>
```

The `div.rightjust` rule does not apply to the paragraph. However, we can simply remove the requirement that it's a div. CSS selectors can simply start with a `#` or a `.` if you don't want it to worry about the category of elements. This way, we can add multiple types of elements to a class.


```html
div#bluediv {
	background-color: skyblue;
}
div.rightjust {    <!-- deleted -->
.rightjust {    <!-- added -->
	text-align: right;
}
</style>
```

Now the selector affects everything inside of that class, regardless of the element's type.

Now we can use the CSS Flexbox layout to put two columns into a container div.

For more examples of Flexbox, look [here](https://css-tricks.com/snippets/css/a-guide-to-flexbox/).

It's fine if this part doesn't totally make sense, I'll probably go over it more in class.


```html
.rightjust {
	text-align: right;
}
.container-2col {    <!-- added -->
	display: flex;    <!-- added -->
	flex-direction: row;    <!-- added -->
}    <!-- added -->
</style>
</head>
<body>
```

```html
	<p class="rightjust">
		This is a paragraph
	</p>
	<div class="container-2col">    <!-- added -->
		<div class="col-left rightjust">    <!-- added -->
			left column    <!-- added -->
		</div>    <!-- added -->
		<div class="col-right">    <!-- added -->
			right column    <!-- added -->
		</div>    <!-- added -->
	</div>    <!-- added -->
</body>
```

We created the "container-2col" class, and we created the "col-left" and "col-right" classes, which we haven't used yet.

So let's create those classes.


```html
	display: flex;
	flex-direction: row;
}
.col-left {    <!-- added -->
	background-color: coral;    <!-- added -->
}    <!-- added -->
.col-right {    <!-- added -->
	background-color: lightgreen;    <!-- added -->
}    <!-- added -->
</style>
</head>
<body>
```

Now we can see the consequences of making those two inner classes.

Now let's experiment with margin and padding.


```html
	flex-direction: row;
}
.col-left {
	margin: 50px;    <!-- added -->
	background-color: coral;
}
.col-right {
	padding: 50px;    <!-- added -->
	background-color: lightgreen;
}
</style>
```

Margin and padding can both give some space around your html element. The difference is if you want the html element to have stylized space, or non-stylized space around it. The outermost layer is padding with no stylization, then margin inside of that, and then the element with text content inside of it.

We can also select every class or id with a word inside of it. Let's modify every class that has "col" in it.


```html
	padding: 50px;
	background-color: lightgreen;
}
[class*="col"] {    <!-- added -->
	font-size: 36;    <!-- added -->
}    <!-- added -->
</style>
</head>
<body>
```

We can see that everything in `container-2col`, `col-left`, and `col-right` has larger text now.