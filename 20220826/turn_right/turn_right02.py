import time

direction_index = 0

direction_list = "North East South West".split()

while True:
    print(
        "Facing {direction}".format(
            direction=direction_list[ direction_index ]
        )
    )
    time.sleep(1)
    direction_index += 1
    direction_index %= len(direction_list)
