import time

trash_list = ["trash"] * 10

direction_index = len(trash_list)

direction_list = trash_list + "North East South West".split()

while True:
    print(
        "Facing {direction}".format(
            direction=direction_list[ direction_index ]
        )
    )
    time.sleep(1)
    direction_index += 1
    direction_index -= len(trash_list)
    direction_index %= len(direction_list) - len(trash_list)
    direction_index += len(trash_list)
