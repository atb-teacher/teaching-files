# Lab 13: ROT Cipher

## NOTE: This lab is subject to change

## A Java Assignment

[hint python file that rotates 2 positions](rot2.py)

Write a program that prompts the user for a string, and encodes it with ROT13. For each character, find the corresponding character, add it to an output string. Notice that there are 26 letters in the English language, so encryption is the same as decryption.


| Index   | 0| 1| 2| 3| 4| 5| 6| 7| 8| 9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|
|---------|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
| English | a| b| c| d| e| f| g| h| i| j| k| l| m| n| o| p| q| r| s| t| u| v| w| x| y| z|
| ROT+13  | n| o| p| q| r| s| t| u| v| w| x| y| z| a| b| c| d| e| f| g| h| i| j| k| l| m|


The default rotation should be 13. Allow the user to input the amount of rotation used in the encryption / decryption, either through arguments or a text prompt.

Allow the user to enter in punctuation, and ignore the punctuation.

If the input is `hello! hello`, the output should be `uryyb! uryyb`. If the input is `AMNZamnz`, the output should be `NZAMnzam`.

## Advanced Version (+1 extra credit)

Achieve the rotation by converting from ascii to a number, adding/subtracting and using modulo, and converting back into ascii.
