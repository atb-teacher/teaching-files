# import os
# os.system("cls")
def rot2(in_char):
    in_num = ord(in_char)
    print(f"{in_char} corresponds to {in_num}")
    num_shifted = in_num - ord('a')
    print(f"{in_char} is letter number {num_shifted + 1}")
    num_rot = num_shifted + 2
    num_rot_mod = num_rot % 26
    print(f"This function should return",
    f"letter number {num_rot_mod + 1}")
    num_output = num_rot_mod + ord('a')
    print(f"Should return ascii char of {num_output}")
    return chr(
        num_output
    )

print(
    rot2('z')
)
