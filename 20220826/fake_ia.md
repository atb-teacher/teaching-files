# Fake IA

## Only for students doing the real IA

## You are not allowed to plagiarize, steal my code, or write fake interviews for the real IA

For the fake IA, you must complete four of the five parts. You must complete parts A, B, C, and E, but you can ignore part D (the video). For guidance, you can look at the rubric, the checklist, and the examples uploaded to Canvas under the `IA` folder. Ignore any guidance about length. You can pretend you've written [this file](randname.py), which is a random name generator. Try using [this json file](example.json) by running `python3 randname.py example.json`.

Learn more about the `randname.py` file by looking at [the commented version of the file](randname_commented.py) or [this debugger](https://pythontutor.com/python-debugger.html#code=import%20random%0Anamelist%20%3D%20%5B'n1',%20'n2'%5D%0Awhile%20True%3A%0A%20%20%20%20active_names%20%3D%20namelist.copy%28%29%0A%20%20%20%20while%20active_names%3A%0A%20%20%20%20%20%20%20%20new_index%20%3D%20random.randrange%28len%28active_names%29%29%0A%20%20%20%20%20%20%20%20print%28active_names.pop%28new_index%29%29%0A%20%20%20%20%20%20%20%20print%28active_names%29%0A%20%20%20%20%20%20%20%20input%28%29&cumulative=false&heapPrimitives=nevernest&mode=edit&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false) which shows the bottom half of the file.

#### A

I am the customer. Create a fake dialogue between you and the customer (me). I need a random name generator, written in python. We can discuss success criteria.

Write a `Planning` document, as outlined in `Criterion A: Planning` in the IA Rubric.

#### B

Create a fake record of tasks. A record of tasks should consist of a table of recorded time-segments when you have been working on different parts of the IA (usually programming or Criterion `A`, `B`, `C`, `D`, or `E`).

Write a `Solution overview` document, as outlined in `Criterion B: Solution overview` in the IA Rubric.


#### C

Write a `Development` document, as outlined in `Criterion C: Development` in the IA Rubric.

#### E

Write an `Evaluation` document, as outlined in `Criterion E: Evaluation` in the IA Rubric.
