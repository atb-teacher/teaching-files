import turtle
from tkinter import *
from tkinter import ttk
root = Tk()
frm = ttk.Frame(root, padding=10)
frm.grid()

def turn_left():
    turtle.left(10)

def turn_right():
    turtle.right(10)

def go_forward():
    turtle.forward(10)

ttk.Button(
        frm, 
        text="<", 
        command=turn_left,
).grid(column=0, row=0)
ttk.Button(
        frm, 
        text="^", 
        command=go_forward,
).grid(column=1, row=0)
ttk.Button(
        frm, 
        text=">", 
        command=turn_right,
).grid(column=2, row=0)

ttk.Button(frm, text="Quit", command=root.destroy).grid(column=0, row=1)
root.mainloop()



