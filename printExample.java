import java.util.Random;
public class printExample {
	// randomly prints sky with or without stars
	// checks every row to see if there are stars in that row
	public static void main(String[] args) {
		Random activeRand = new Random();
		char[][] sky = new char[4][4];
		for (int i=0; i<4; i++) {
			for (int j=0; j<4; j++) {
				sky[i][j] = activeRand.nextInt(6) > 4 ? '*' : ' ';
			}
		}
		for (int i=0; i<4; i++) {
			for (int j=0; j<4; j++) {
				System.out.print(sky[i][j]);
			}
			printHasStar(sky[i]);
			System.out.println();
		}
	}
	public static void printHasStar(char[] inArr) {
		String output = "no";
		for (int i=0; i<inArr.length; i++) {
			if (inArr[i] == '*') {
				output = "yes";
				break;
			}
		}
		System.out.print(output);
	}
}
