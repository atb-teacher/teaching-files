# Lab: Letter Grade

Let's create a method that takes in a number grade as an integer, and prints out the corresponding letter grade (returning instead of printing is also acceptable).

Use `if`, `else if`, and `else` to convert an integer grade to a letter grade.

## Ranges Corresponding to Each Letter

- 90-100: A
- 80-89: B
- 70-79: C
- 60-69: D
- 0-59: F

## Bonus Goal 1

Use a switch statement instead of a series of if statements, or use a switch statement in combination with if statements.

## Bonus Goal 2

Use a random number to represent a rival's score. Use this code to get a random integer:

    jshell> Random myRand = new Random();
    jshell> myRand.nextInt(3)

The above example will give you a random integer between zero and two, so it'll give you either 0, 1, or 2. `nextInt(x)` will give a random integer between zero and `x-1`.

Using `nextInt`, give the rival a random score between 50 and 100. Let the user know if they beat their rival.

## Bonus Goal 3

Use `%` to get the remainder of the grade when divided by ten, which is the same as the number in the ones digit. The number in the ones digit will determine whether they will get a '+' or a '-' appended to the end of their grade. For example, the grade `81` would be a 'B'. `81 % 10` would give you 1, which is a low number, so you would add a '-' to the end of the grade.
