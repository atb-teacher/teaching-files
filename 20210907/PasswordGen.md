# Lab: Password Generator

Let's create a password generator, that will give us a password that is 10 characters long. You can use this string:

    abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~

## Bonus Goal 1

Allow the method that creates the password to take in an integer that determines the length.

## Bonus Goal 2

Try using only one print statement to print out the password. Here's a bit of python code as a hint on how to do that:

    >>> my_str = ''
    >>> counter = 0
    >>> while counter < 5:
    ...     my_str = my_str + 'a'
    
    ...     counter = counter + 1
    ... 
    >>> print(my_str)
    aaaaa


## Bonus Goal 3

Allow the method to take in the number of numbers, the number of letters, and the number of punctuation marks, and make a password that combines all of those.
