# Linked List

Here's an example of a simple Linked List in python:

    class Node:
        def __init__(self, datum):
            self.datum = datum
            self.next = None
    
        def add(self, datum):
            if self.next is None:
                self.next = Node(datum)
            else:
                self.next.add(datum)
    
        def print(self):
            print(self.datum, end=" ")
            if self.next is not None:
                self.next.print()
    
    my_node = Node('a')
    my_node.add('b')
    my_node.add('c')
    my_node.add('d')
    my_node.print()

Try implementing a similar Linked List in Java.

(Note: Often Linked Lists have specific classes for the head and tail, which are the first and last nodes, respectively)

## Bonus Goal 1

Try to add methods that allow deleting a node at a specific index and inserting a node with a datum at a specific index.

## Bonus Goal 2

Make a Doubly Linked List, which is a Linked List where every node refers to the node before it and the node after it.

## Bonus Goal 3

Make a splice method that returns an array of nodes.
