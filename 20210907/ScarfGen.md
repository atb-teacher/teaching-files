# Guide: Scarf Gen

Let's make a scarf generator, that prints out pretty scarf patterns.

We'll start with our file, `ScarfGen.java`:

    public class ScarfGen {

           public static void main(String args[]) {
           }

    }

We can give our `ScarfGen` some basic patterns to remember, like this:


    public class ScarfGen {

           private String[] patternArray = {"##", "$$", "//", "\\", "%%"}; // new line

           public static void main(String args[]) {
	   }    
    }

Now we have an array of strings, that is `private`. This means that we can access the string inside of this class, but other classes can't access it. We also won't be able to access it through the static method `main`.

Now we can create a method that uses `patternArray` to print a beautiful scarf.

    public class ScarfGen {
    
           private String[] patternArray = {"##", "$$", "//", "\\", "%%"};
    
           public void makeScarf(int length) { // new line
                   for (int i=0; i<length; i++) { // new line
                           System.out.println(patternArray[0]); // new line
                   } // new line
           } // new line
    
           public static void main(String args[]) {
                   ScarfGen myScarfGen = new ScarfGen(); // new line
                   myScarfGen.makeScarf(10); // new line
           }

    }


Well, the scarf isn't too beautiful right now. We're printing out the same pattern, `##`, and it's much too narrow. We can fix both of these problems.

First we'll need to figure out how to randomly access something from the array. Fortunately, we can use Java's `Random` class which has the method `nextInt`, and that should give us a random integer between 0 and the number we give it. It can randomly give us 0, but it cannot randomly give us the number we give it.

Because the indecies of an array are always `0` through `the array's length - 1`, we should be able to put the array's length into `nextInt`, and it'll give us a random index we can use to access the array.

This part should give us a new error, though.


    import java.util.Random; // new line
    
    public class ScarfGen {
    
           private String[] patternArray = {"##", "$$", "//", "\\", "%%"};
    
           public void makeScarf(int length) {
                   Random myRand = new Random(); // new line
                   int index; // new line
                   for (int i=0; i<length; i++) {
                           System.out.println(patternArray[0]); // deleted line
                           index = myRand.nextInt(patternArray.length); // new line
                           System.out.println(patternArray[index]); // new line
                   }
           }
    ...

If you run this, you might notice that the backslashes only print one character, making them thinner than all the other sections of the scarf that print out. This is because the backslash is an escape character. In a string, a backslash is generally going to look forward to the next character and try to interpret that character in a special way. Here, the backslash finds another backslash, and helps it to create a non-escape-character backslash, or a backslash that has been escaped.

We will have to make the backslash section of our array longer.

We also need to complete the final step to make the scarf generator: we need to make it wider.

    public class ScarfGen {
    
           private String[] patternArray = {"##", "$$", "//", "\\", "%%"}; // deleted line
           private String[] patternArray = {"##", "$$", "//", "\\\\", "%%"}; // new line
    
           public void makeScarf(int length) {
                   Random myRand = new Random();
                   int index;
                   for (int i=0; i<length; i++) {
                           index = myRand.nextInt(patternArray.length);
                           System.out.println(patternArray[index]); // deleted line
                           System.out.println(patternArray[index].repeat(5)); // new line
                   }
           }
    ...


And now we should see a beautiful scarf that looks like this:


    //////////
    //////////
    \\\\\\\\\\
    $$$$$$$$$$
    ##########
    ##########
    //////////
    \\\\\\\\\\
    \\\\\\\\\\
    ##########
