# Guide: Turn Right

Let's make a program that tells the user what cardinal direction they are facing, lets them turn 90 degrees right how many times they want, and then tells them the direction they are facing again.

We will start with an array of strings in our class `TurnRight`, and an integer to represent the direction the user is facing:


    public class TurnRight {
    
           private String[] directions = {"North", "East", "South", "West"};
           private int currentDirection = 0;
    
           public static void main(String[] args) {
           }

    }

Now we can add two methods: `Turn` will change the number that represents where the user is facing, and `announceDirection` will use the number and the direction array to announce the user's current direction:

    public class TurnRight {
           private String[] directions = {"North", "East", "South", "West"};
           private int currentDirection = 0; // new line
           public void Turn(int turnNum) { // new line
                   this.currentDirection += turnNum; // new line
           } // new line
           public void announceDirection() { // new line
                   System.out.println(this.directions[this.currentDirection]); // new line
           } // new line
           public static void main(String[] args) {
    
                   TurnRight myTurn = new TurnRight(); // new line
                   myTurn.announceDirection(); // new line
                   myTurn.Turn(3); // new line
                   myTurn.announceDirection(); // new line
           }
    }

This program should work, announcing that we are facing `North` and then `West`. However, we can break this program easily.

    ...
           public static void main(String[] args) {
                   TurnRight myTurn = new TurnRight();
                   myTurn.announceDirection();
                   myTurn.Turn(3); // deleted line
                   myTurn.Turn(5); // new line
                   myTurn.announceDirection();
           }
    ...

Now our program should say that we've turned too far, and it's confused. We have given our array a number that's too big.

Our array can receive the numbers 0 through 3. If our counter goes beyond that, then our array cannot use that counter as an index. It would be great if we could keep our counter in that range. Fortunately, we can do it in a less efficient way and a more efficient way.

We really need to subtract 4 a bunch of times if our counter gets too big. We can think of it like this:

    jshell> int counter = 401
    counter ==> 401
    
    jshell> while (counter >= 4) {
       ...>     counter -= 4;
       ...> }
    
    jshell> counter
    counter ==> 1

Fortunately, there's a cleaner way to do that same thing:

    jshell> int counter = 401
    counter ==> 401
    
    jshell> counter %= 4;
    
    jshell> counter
    counter ==> 1

So the `modulous`, represented by `%`, will subtract 4 for us and keep us in the magic range. Of course, we should try not to have the number 4 in our code. Having random values in your code is unclear, and it makes the program more likely to break. It's entirely possible that we'll want to change the program in the future to have 8 directions, so that the user can turn 45 degrees and change from North to Northeast. In that case, all our hardcoded `4`s will be a pain to track down. Instead of `4` we can use `directions.length`, which is clearer, and which will change if we modify `directions`.


    ...
           public void Turn(int turnNum) {
                   this.currentDirection += turnNum;
                   this.currentDirection %= this.directions.length; // new line
           }
    ...

Now our Turn Right program should work great!
