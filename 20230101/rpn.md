# Reverse Polish Notation Calculator

## A Java Lab

You are to make a `Reverse Polish Notation (RPN)` calculator. It will take in arguments and parse them, and print out the result.

We are used to `infix notation`, which we see when we look at ` 3 - 4 + 5`. To convert this to `RPN`, all we need to do is imagine the order of operations, and for each operation put it in `number number operator` order. To evaluate the input, we need to first figure out `3 - 4`, and then the resulting number will be added to `5`. So in `RPN` we can type `3 4 - 5 +`.

To do this calculation, we will create a `Stack` class (note: Java already has a `Stack` class, but we will create our own `Stack` class that is different from the provided class).

The stack should have the following attributes:

```java
private double[]  ar_stack; 
private int tos = -1; // this is top of stack. It always indicates the location of the most recent addition to the stack. 
```

`ar_stack` should be able to hold 100 items.


The stack should have the following methods:
```java
public void push(double item_to_push); 
 
public double pop(); 
 
public void dumpstack();
```

The push and pop methods should check for overflow and underflow respectively.  `push` puts 
an item on the stack.  `pop` removes the item at the top of stack and returns it.  `dumpstack` 
prints horizontally on the line the items in the stack from bottom to top. 

When a number is received it should be pushed on the stack.
 
When an operator (`+`, `-`, `x`, `/`) is received your program should pop the two top numbers, do the appropriate arithmetic operation and pushes the result on the stack.   

Example:

>Suppose we had an instance of Stack called my_stack. Suppose the user typed in `8 2 +`. Then, the action for the `+` sign would be
>>my_stack.push(my_stack.pop() + my_stack.pop()); 
 

If your program is run with zero arguments, it should run as a `REPL`. If your program is run with `-i`, it should receive the numbers and operators following `-i` and output the result without a `REPL`.

When this is done I should be able to enter the following and get the correct result:

When this is done I should be able to enter `main.java 66 8 5 5 \* + /` and get 2.0 as a result.

![This is how you should visualize the math](stack.png).

Example of printing args:

```java
public class example {
        public static void main(String[] args) {
                for (int i=0; i<args.length; i++) {
                        System.out.println(args[i] + ":)");
                }
        }
}
```

```
$ java example a b c hello
a:)
b:)
c:)
hello:)
```
