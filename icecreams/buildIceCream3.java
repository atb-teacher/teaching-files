public class buildIceCream3 {
	public static void main(String args[]) {
		IceCream myIce1 = IceCream.builder().setFlavor("Cherry").setTopping("Gummy Bears").build();
		myIce1.print();
		IceCream myIce2 = IceCream.builder().build();
		myIce2.print();
	}
}


class IceCream {
	public static IceCreamBuilder builder() {
		return new IceCreamBuilder();
	}
	static class IceCreamBuilder {
		String flavor = "vanilla";
		String base = "cone";
		String topping = "sprinkles";
		public IceCreamBuilder setFlavor( String flavorParam ) {
			this.flavor = flavorParam;
			return this;
		}
		public IceCreamBuilder setBase( String baseParam ) {
			this.base = baseParam;
			return this;
		}
		public IceCreamBuilder setTopping( String toppingParam ) {
			this.topping = toppingParam;
			return this;
		}
		public IceCream build() {
			return new IceCream( this );
		}
	}
	String flavor;
	String base;
	String topping;
	public IceCream( IceCreamBuilder builderParam ) {
		this.flavor = builderParam.flavor;
		this.base = builderParam.base;
		this.topping = builderParam.topping;
	}
	public void print() {
		System.out.println(
			String.format("%s ice cream ", this.flavor) + 
			String.format("with a %s ", this.base) + 
			String.format("with %s on top", this.topping)
		);
	}
}
