public class buildIceCream1 {
	public static void main(String args[]) {
		System.out.println("hi");
		IceCream myIce = new IceCream();
		myIce.print();
	}
}
class IceCream {
	String flavor;
	String base;
	String topping;
	public IceCream() {
		this.flavor = "vanilla";
		this.base = "cone";
		this.topping = "sprinkles";
	}
	public void print() {
		System.out.println(
			String.format("%s ice cream ", this.flavor) + 
			String.format("with a %s ", this.base) + 
			String.format("with %s on top", this.topping)
		);
	}
}
