public class buildIceCream2 {
	public static void main(String args[]) {
		IceCreamBuilder myBuild = new IceCreamBuilder();
		IceCream myIce1 = new IceCream( myBuild );
		myIce1.print();
		myBuild.setFlavor("Cherry Garcia");
		IceCream myIce2 = new IceCream( myBuild );
		myIce2.print();
		IceCream myIce3 = new IceCream( myBuild );
		myIce3.print();
	}
}

class IceCreamBuilder {
	String flavor = "vanilla";
	String base = "cone";
	String topping = "sprinkles";
	public IceCreamBuilder setFlavor( String flavorParam ) {
		this.flavor = flavorParam;
		return this;
	}
}

class IceCream {
	String flavor;
	String base;
	String topping;
	public IceCream( IceCreamBuilder builderParam ) {
		this.flavor = builderParam.flavor;
		this.base = builderParam.base;
		this.topping = builderParam.topping;
	}
	public void print() {
		System.out.println(
			String.format("%s ice cream ", this.flavor) + 
			String.format("with a %s ", this.base) + 
			String.format("with %s on top", this.topping)
		);
	}
}
