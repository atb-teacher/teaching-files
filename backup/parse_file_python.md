# Parse a file with Python
	with open("data.txt", "r") as f:
		for line in f.readlines():
			print(line)



	import re # added line
	with open("data.txt", "r") as f:
	 	for line in f.readlines():
			print(line) # deleted line
			one_match = re.match(r'^.', line) # added line
			print(one_match.group()) # added line



	 
	 
	 with open("data.txt", "r") as f:
	 	for line in f.readlines():
			one_match = re.match(r'^.', line) # deleted line
			print(one_match.group()) # deleted line
			one_match = re.match(r'^(.)..(.)', line) # added line
			print(one_match.groups(), one_match.group()) # added line




	import re
	 
	with open("data.txt", "r") as f:
		for line in f.readlines():
			one_match = re.match(r'^(.)..(.)', line) # deleted line
			one_match = re.match(r'^([A-Za-z]+)\s([A-Za-z]+)', line) # added line
	 		print(one_match.groups(), one_match.group())






	import re
	 
	with open("data.txt", "r") as f:
	 	for line in f.readlines():
			one_match = re.match(r'^([A-Za-z]+)\s([A-Za-z]+)', line) # deleted line
			print(one_match.groups(), one_match.group()) # deleted line
			one_match = re.match(r'^(?P<first_name>[A-Za-z]+)\s(?P<last_name>[A-Za-z]+)', line) # added line
			print(one_match.group('first_name'), one_match.group('last_name')) # added line





	import re
	 
	with open("data.txt", "r") as f:
	 	for line in f.readlines():
			one_match = re.match(r'^(?P<first_name>[A-Za-z]+)\s(?P<last_name>[A-Za-z]+)', line) # deleted line
			print(one_match.group('first_name'), one_match.group('last_name')) # deleted line
			matches = re.match(r'^(?P<first_name>[A-Za-z]+)\s(?P<last_name>[A-Za-z]+)\s+(?P<age>\d+)', line) # added line
			print(matches.group('first_name'), matches.group('last_name'), f"is {matches.group('age')} years old.") # added line





	import re
	 
	with open("data.txt", "r") as f:
		for line in f.readlines(): # deleted line
		for line in f.readlines()[1:]: # added line
	 		matches = re.match(r'^(?P<first_name>[A-Za-z]+)\s(?P<last_name>[A-Za-z]+)\s+(?P<age>\d+)', line)
	 		print(matches.group('first_name'), matches.group('last_name'), f"is {matches.group('age')} years old.")






	import re
	 
	with open("data.txt", "r") as f:
	 	for line in f.readlines()[1:]:
			matches = re.match(r'^(?P<first_name>[A-Za-z]+)\s(?P<last_name>[A-Za-z]+)\s+(?P<age>\d+)', line) # deleted line
			print(matches.group('first_name'), matches.group('last_name'), f"is {matches.group('age')} years old.") # deleted line
			matches = re.match(r'^(?P<first_name>[A-Za-z]+)\s(?P<last_name>[A-Za-z]+)\s+(?P<age>\d+)\s+(?P<cards>\d+)$', line) # added line
			print(matches.group('first_name'), matches.group('last_name'), f"is {matches.group('age')} years old,", end=' ') # added line
			print(f"and they have {matches.group('cards')} pokemon cards.") # added line




	import re
	 
	with open("data.txt", "r") as f:
		for line in f.readlines()[1:]: # deleted line
		f.readline() # added line
		while True: # added line
			line = f.readline() # added line
			if line is None or not line: # added line
				break # added line
	 		matches = re.match(r'^(?P<first_name>[A-Za-z]+)\s(?P<last_name>[A-Za-z]+)\s+(?P<age>\d+)\s+(?P<cards>\d+)$', line)
	 		print(matches.group('first_name'), matches.group('last_name'), f"is {matches.group('age')} years old,", end=' ')
	 		print(f"and they have {matches.group('cards')} pokemon cards.")