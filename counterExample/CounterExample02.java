public class CounterExample02 {
	public static void main(String[] args) {
	int[] highway = {1, 2, 7, 8, 9, 9, 7, 9, 2, 1, 2};
	SpillCounter myCounter = new SpillCounter();
	int[] output = myCounter.checkSpill(highway);
	System.out.println(
		String.format(
			"Spill starts at %d, ends at %d",
			output[0],
			output[1]
		)
	);
	}
}

class SpillCounter {
	public int start;
	public int stop;
	public SpillCounter() {
		start = -1;
		stop = -1;
	}
	public int[] checkSpill(int[] highway) {
		int[] output = checkSpillStep(false, -1, 0, highway);
		start = output[0];
		stop = output[1];
		return output;
	}
	public int[] checkSpillStep(boolean foundOil, int start, int loc, int[] highway) {
		int reflectiveness = highway[loc];
		if (!foundOil) {
			if (reflectiveness > 5) {
				start = loc;
				return checkSpillStep(true, loc, loc + 1, highway);
			}
			else {
				return checkSpillStep(false, start, loc + 1, highway);
			}
		}
		else {
			if (reflectiveness < 5) {
				stop = loc - 1;
				int[] output = {start, stop};
				return output;
			}
			else {
				return checkSpillStep(true, start, loc + 1, highway);
			}
		}

	}
}
