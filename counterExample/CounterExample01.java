public class CounterExample01 {
	public static void main(String[] args) {
	int[] highway = {1, 2, 7, 8, 9, 9, 7, 9, 2, 1, 2};
	SpillCounter myCounter = new SpillCounter();
	int[] output = myCounter.checkSpill(highway);
	System.out.println(
		String.format(
			"Spill starts at %d, ends at %d",
			output[0],
			output[1]
		)
	);
	}
}

class SpillCounter {
	public int start;
	public int stop;
	public SpillCounter() {
		start = -1;
		stop = -1;
	}
	public int[] checkSpill(int[] highway) {
		start = -1;
		stop = -1;
		boolean foundReflective = false;
		int reflectiveness;
		for (int i =0; i<highway.length; i++) {
			reflectiveness = highway[i];
			if (!foundReflective) {
				if (reflectiveness > 5) {
					foundReflective = true;
					start = i;
				}
			}
			else {
				if (reflectiveness < 4) {
					stop = i-1;
					break;
				}
			}
		}
		int[] output = {start, stop};
		return output;
	}
}
