public class CounterExample03 {
	public static void main(String[] args) {
	int[] highway = {1, 2, 7, 8, 9, 9, 7, 9, 2, 1, 2};
	SpillCounter myCounter = new SpillCounter();
	myCounter.checkSpill(highway);
	System.out.println(
		String.format(
			"Spill starts at %d, ends at %d",
			myCounter.start,
			myCounter.stop
		)
	);
	}
}

class SpillCounter {
	public int start;
	public int stop;
	public int loc;
	public boolean foundOil;
	public SpillCounter() {
		start = -1;
		stop = -1;
		loc = 0;
		foundOil = false;
	}
	public void checkSpill(int[] highway) {
		SpillCounter output = checkSpillStep(this, highway);
		start = output.start;
		stop = output.stop;
		loc = output.loc;
		foundOil = output.foundOil;
	}
	public SpillCounter checkSpillStep(SpillCounter ctr, int[] highway) {
		int reflectiveness = highway[ctr.loc];
		if (!ctr.foundOil) {
			if (reflectiveness > 5) {
				ctr.start = ctr.loc;
				ctr.foundOil = true;
				ctr.loc += 1;
				return checkSpillStep(ctr, highway);
			}
			else {
				ctr.loc += 1;
				return checkSpillStep(ctr, highway);
			}
		}
		else {
			if (reflectiveness < 5) {
				ctr.stop = ctr.loc - 1;
				return ctr;
			}
			else {
				ctr.loc += 1;
				return checkSpillStep(ctr, highway);
			}
		}

	}
}
