// MAP EXAMPLE

import java.util.stream.*;
import java.util.*;
class StreamsExample01 {
	public static void main(String[] args) {
		List<Integer> numbers = Arrays.asList(2, 3, 8, 7, 6);
		List<Integer> numbersMapped = numbers
			.stream()
			.map(x->x+1)
			.collect(Collectors.toList());

		System.out.println(
			"Map: "
		);
		for (int i=0; i<numbersMapped.size(); i++) {
			System.out.println(numbersMapped.get(i));
		}
	}
}
