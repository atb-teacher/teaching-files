// FILTER EXAMPLE

import java.util.stream.*;
import java.util.*;
class StreamsExample02 {
	public static void main(String[] args) {
		List<Integer> numbers = Arrays.asList(2, 3, 8, 7, 6);
		List<Integer> numbersFiltered = numbers
			.stream()
			.filter(x -> x % 2 == 0)
			.collect(Collectors.toList());

		System.out.println(
			"Map: "
		);
		for (int i=0; i<numbersFiltered.size(); i++) {
			System.out.println(numbersFiltered.get(i));
		}
	}
}
