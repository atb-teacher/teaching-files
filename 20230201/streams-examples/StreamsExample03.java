// REDUCE EXAMPLE

import java.util.stream.*;
import java.util.*;
class StreamsExample03 {
	public static void main(String[] args) {
		List<Integer> numbers = Arrays.asList(2, 3, 8, 7, 6);
		Integer numbersReduced = numbers
			.stream()
			.reduce(0, (ans, num) -> ans + num);

		System.out.println(
			"Reduce: "
		);
		System.out.println(
			numbersReduced	
		);
	}
}
