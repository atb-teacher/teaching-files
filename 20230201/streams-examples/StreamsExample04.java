// MAX EXAMPLE

import java.util.stream.*;
import java.util.*;
class StreamsExample04 {
	public static void main(String[] args) {
		List<Integer> numbers = Arrays.asList(2, 3, 8, 7, 6);
		Integer numbersMax = numbers
			.stream()
			.max(Integer::compare)
			.get();

		System.out.println(
			"Max: "
		);
		System.out.println(
			numbersMax
		);
	}
}
