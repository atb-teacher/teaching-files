import java.util.Scanner;
import java.util.ArrayList;

public class MyUML {
	public static void main(String[] args) {
		Scanner myScan = new Scanner(System.in);
		String response;
		GenericUser user = null;
		while (user == null) {
			// ask if they are local/foreign, create the correct user
			// I'll just cheat and assign it to NationalUser
			user = new NationalUser("Sam");
		}
		String[] items = {"basketball", "computer", "water bottle"};
		/* 
		 * This should be an ArrayList
		 * with items from a class "Item"
		 * that I'm not going to write
		*/
		Order mainOrder = user.makeOrder(items);
		mainOrder.printInfo();
	}
}
class Order {
	private ArrayList<String> itemArr; // this should contain Items instead of Strings
	Order() {
		itemArr = new ArrayList<String>();
	}
	public void addItem(String itemParam) {
		// this should take in an Item, not a String
		itemArr.add(itemParam);
	}
	public void printInfo() {
		itemArr.forEach(
			(item) -> System.out.println(item)
		);
	}

}

class GenericUser {
	String name;
	boolean isLocal;

	public Order makeOrder(String[] items) {
		/*
		 * This should take in an arraylist of items from the Item class
		*/
		Order returnedOrder = new Order();
		for (int i=0; i<items.length; i++) {
			returnedOrder.addItem(items[i]);
		}
		return returnedOrder;
	}
}

class NationalUser extends GenericUser {
	public NationalUser(String nameParam) {
		// probably needs more parameters
		name = nameParam;
		isLocal = false;
	}
}

/*
 * class Item {
 * 	this class should exist
 * }
 */
