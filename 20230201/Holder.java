public class Holder<Generic> {

	public Generic held;

	public Holder(Generic g) {
		held = g;
	}

	public static void main(String[] args) {

		// HOLD AN INTEGER
		Holder h1 = new Holder<Integer>(5);
		System.out.println(h1.held);

		// HOLD A STRING
		Holder h2 = new Holder<String>("hello");
		System.out.println(h2.held);

	}

}
