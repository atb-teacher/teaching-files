heights = [3, 2, 1, 2, 3, 4, 5, 4, 3, 2, 1]
for y_pos in reversed(range(10)): # 9 through 0
    for x_pos in range(len(heights)):
        if y_pos > heights[x_pos]:
            print(".", end=" ")
        else:
            print("x", end=" ")
    print()
