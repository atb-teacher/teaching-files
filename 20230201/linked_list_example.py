# MAKE A LINKED LIST IN PYTHON

class Node:
    def __init__(self, datum):
        """A Linked List has a datum and a pointer to the next node"""
        self.datum = datum
        self.next = None

    def add(self, datum):
        """pass the datum to the end of the linked list,
        and then create a new node"""
        if self.next is None:
            self.next = Node(datum)
        else:
            self.next.add(datum)

    def print(self):
        """print each datum and then go to the next node"""
        print(self.datum, end=" ") # we don't want a new line
        if self.next is not None:
            self.next.print()
        print()

my_node = Node(5)
my_node.add('a')
my_node.add(True)

my_node.print()
