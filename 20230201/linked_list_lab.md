# Linked List Lab
## A Java lab

Implement a linked list in Java, with an `add` method that adds an object to the end of the list, and a `print` method that prints out every object. It should look like [this](linked_list_example.py).

Use generics, so any object can be added to the linked list. [Generics hint](Holder.java)
