# ASCII Mountain with Rainfall
## A Java lab

You're giving an array of integers:

> {2, 3, 4, 5, 6, 5, 4, 5, 6, 7, 8, 9, 8, 7, 6, 5, 4, 3, 2, 3, 4, 3, 2, 3, 4, 5}

This array represents different heights in a mountain range.

## Part 1

Print out the mountain range with ASCII. You can use `X`s to represent mountain, and `.`s to represent sky, or something similar.

```
..........................
...........x..............
..........xxx.............
.........xxxxx............
....x...xxxxxxx...........
...xxx.xxxxxxxxx.........x
..xxxxxxxxxxxxxxx...x...xx
.xxxxxxxxxxxxxxxxx.xxx.xxx
xxxxxxxxxxxxxxxxxxxxxxxxxx
xxxxxxxxxxxxxxxxxxxxxxxxxx
```

## Part 2

Make your program print out the indecies of all peaks and valleys.

```
peaks at indecies 4 11 20
valleys at indecies 6 18 22
```

## Part 3

Include a "rain" option to print out the mountain range post-rain.

```
..........................
...........x..............
..........xxx.............
.........xxxxx............
....x~~~xxxxxxx...........
...xxx~xxxxxxxxx~~~~~~~~~x
..xxxxxxxxxxxxxxx~~~x~~~xx
.xxxxxxxxxxxxxxxxx~xxx~xxx
xxxxxxxxxxxxxxxxxxxxxxxxxx
xxxxxxxxxxxxxxxxxxxxxxxxxx
```
