using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class updateText : MonoBehaviour
{
    public GameObject sphere;
    public Text txt;
    private playerMovement movement_script;
    // Start is called before the first frame update
    void Start()
    {
        movement_script = sphere.GetComponent<playerMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        txt.text = $"Score: {movement_script.score}";
    }
}
