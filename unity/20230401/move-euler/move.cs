using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class move : MonoBehaviour
{
    public Rigidbody rb;
    public float speed = 10f;
    public Transform cam;

    void Update()
    {
        float horizontal = Input.GetAxis("Horizontal"); // picking up left/right
        float vertical = Input.GetAxis("Vertical"); // picking up up/down
        Vector3 direction = new Vector3(horizontal, 0f, vertical); // creating a normalized direction
        Vector3 moveDir = Quaternion.Euler(0f, cam.eulerAngles.y, 0f) * direction;
        rb.AddForce (moveDir * speed);
        print(moveDir);
    }
}
