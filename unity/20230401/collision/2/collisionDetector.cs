using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collisionDetector : MonoBehaviour
{
    public Transform cube;
    public float distanceCheck = 20;
    public bool isClose;
    public LayerMask groundMask;
    public Vector3 velocity;
    // Start is called before the first frame update
    void Start()
    {
        velocity = new Vector3(0f, -0.5f, 0f);
    }

    // Update is called once per frame
    void Update()
    {
        isClose = Physics.CheckSphere(
                transform.position,
                distanceCheck,
                groundMask
            );
        print( isClose );
        transform.Translate(velocity * Time.deltaTime);
    }
}
