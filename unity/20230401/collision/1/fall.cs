using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fall : MonoBehaviour
{
    public CharacterController controller;
    public float gravity = -9.81f;
    public float startHeight = 3f;
    Vector3 velocity;
    bool isGrounded;
    public LayerMask groundMask;
    public Transform groundCheck;
    public float groundDistance = 0.4f;
    //public int counter = 0;
    void Start() {
        /*
        transform.position = new Vector3(
            transform.position.x,
            startHeight,
            transform.position.z);

        */
        velocity = new Vector3(0f, 0f, 0f);
    }
    void Update()
    {
        //counter += 1;
        //if (counter % 20 == 0) {
            isGrounded = Physics.CheckSphere(transform.position, groundDistance, groundMask);
            if (isGrounded && velocity.y < 0) {
                velocity.y = 0f;
            }
            else {
                velocity.y += gravity * Time.deltaTime;
            }
            print(isGrounded);
            transform.Translate(velocity * Time.deltaTime);
        //}
    }
}
