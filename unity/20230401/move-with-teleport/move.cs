// move.cs

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class move : MonoBehaviour
{
    public Rigidbody rb;
    public float speed = 10f;
    public Transform cam;
    int score = 0;
    public Text txt;
    void Update()
    {
        float horizontal = Input.GetAxis("Horizontal"); // picking up left/right
        float vertical = Input.GetAxis("Vertical"); // picking up up/down
        Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized; // creating a normalized direction
        Vector3 moveDir = Quaternion.Euler(0f, cam.eulerAngles.y, 0f) * direction; // rotating based on camera position
        rb.AddForce (moveDir * speed);
        print(moveDir);
    }
    void OnTriggerEnter(Collider Other) { // When you collide with something
        if ( Other.gameObject.CompareTag("TeleIn") ) { // If it has the tag "TeleIn"
            score = score + 1;
            txt.text = `Score: ${score}`;
            transform.position = Other.gameObject.transform.GetChild(0).transform.position; // Go to the child's position
        }
    }
}