import os
this_file = os.path.abspath(__file__)
rjust_int = 20
print("this file:".rjust(rjust_int), this_file)
parent_folder = os.path.dirname(this_file)
print("this folder:".rjust(rjust_int), parent_folder)
nearby_file = os.path.join(parent_folder, "example.csv")
print("example file:".rjust(rjust_int), nearby_file)
