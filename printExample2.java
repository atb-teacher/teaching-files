import java.util.Random;
public class printExample2 {
	public static void main(String[] args) {
		Random activeRand = new Random();
		int[] sky = new int[4];
		// make the starry sky
		// it's an array of numbers between 6 and 9 inclusive
		// to represent the height of each star
		for (int i=0; i<4; i++) {
			sky[i] = activeRand.nextInt(4) + 6;
		}
		// print the starry sky
		for (int height = 10; height >=0; height--) {
			for (int i=0; i<4; i++) {
				if (sky[i] == height) {
					System.out.print('*');
				}
				else {
					System.out.print(' ');
				}
			}
			System.out.println();
		}
	}
}
