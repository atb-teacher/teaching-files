# Say what these bits of sample code will print out

1.

	public class Main {
		public static void main(String args[]) {
			int counter = 0;
			for (int i=0; i<10; i++) {
				counter++;
			}
			System.out.println(counter);
		}
	}

2.

	public class Main {
		public static void main(String args[]) {
			int index = 3;
			String abcs = "abcdefg";
			char[] ch = abcs.toCharArray();
			System.out.println(ch[index]);
		}
	}

3.

	public class Main {
		public static void main(String args[]) {
			int index = 3;
			String abcs = "abcdefg";
			char[] ch = abcs.toCharArray();
			if (index > 4) {
				System.out.println(ch[index]);
			} else {
				System.out.println(ch[index + 1]);
			}
		}
	}


4.

	public class Main {
		public static void main(String args[]) {
			int index = 3;
			int[] twos = {0, 2, 4, 6, 8, 10, 12};
			System.out.println(twos[index] + 1);
		}
	}


5.

	public class Main {
		public static void main(String args[]) {
			int index = 3;
			String abcs = "abcdefg";
			char[] ch = abcs.toCharArray();
			for (int i=0; i<ch.length; i++) {
				if (i<3) {
					System.out.println(ch[i]);
				}
			}
		}
	}
