# Practice Test \#1 Answers

### Lecture

1. Imagine two scenarios

    Adding to the linked list would be more efficient. When an array is filled with information, adding to the beginning is inefficient because it involves shifting all the info one place. When adding to a linked list, you can just change the pointers.

1. Do a bitwise `xor` operation between these two binary numbers:

    * `11110000`
    * `10101010`

    Gives you `01011010` you look at each digit. For that digit, if the numbers are different, then `xor` is true, so you write `1`. If the numbers are the same, you write `0`.

1. You can only create a hash for which of the following:
    * Mutable (changeable) data structures
    * Immutable (unchangeable) data structures
    
    I won't ask about hashes, ignore this

1. Hashes are used in which data structures (mark all that apply):
    * Linked Lists
    * Sets
    * Arrays
    * Hashmaps/Dictionaries

    I won't ask about hashes, ignore this

### Sorting algorithms

1. Which is the most efficient sorting algorithm of the three we learned?

    * MergeSort

1. Which sorting algorithm involves comparing each item to nearby items, and making them step incrementally?

    * BubbleSort

1. Which sorting algorithm uses a "guess and check" method of sorting?

    * BogoSort

1. Which sorting algorithm involves breaking the sequence down into smaller segments, and then recombining them?

    * MergeSort

### Chapter 1

1. Fill out the components of the software lifecycle (1.1): 
    
    ![software lifecycle](image17.png)
    
    I'll try to come up with a simpler question than this.

1. Which of the following is SaaS? (1.1.4)
    * You pay a company so that you have access to a server, where you can host a database.
    * You pay a company to download and use their database management software.
    * You pay a company to access their online platform, which manages a database.

    The answer is:

    * You pay a company to download and use their database management software.

    Using software indicates Software as a Service. The other two would be Platform as a Service and Infrastructure as a Service.

1. What is the difference between verification and validation? (1.1.7)

    Verification means you have specific standards for software, whereas validation is making sure that it satisfies the proper design function

1. Match the terms and the descriptions (1.2.1):

    Terms:

    A. Peripheral Device

    B. Computer Network

    C. Human Resource

    D. Hardware

    E. Software

    Descriptions:

    1. The physical elements of a computer (screen, CPU, etc) - D
    1. A series of instructions that can be understood by the CPU - E
    1. Any auxilliary device - A
    1. A set of computer systems that are interconnected and share resources - B
    1. People who can be useful - C

1. Match the terms and the descriptions (1.2.1):

    Terms:

    A. client

    B. Domain Name System Server
    
    C. router

    D. dumb terminal

    E. firewall

    F. email server

    G. client-server

    H. host

    I. thin client

    Descriptions:

    1. A device that only consists of a keyboard, a monitor, and a network card - D
    1. A relatively low performance terminal - I
    1. Receives data from the network - A
    1. Provides data or services to the network - H
    1. Allows users to access their email - F
    1. Networking device that accepts incoming data packets and distributes them across networks - C
    1. Links website names with network addresses, so that you don't have to directly type IP addresses into your browser - B
    1. Hardware or software that protects a network by limiting access - E
    1. An architecture system where clients request information and servers provide information - G

1. Match the terms and the descriptions (1.2.12):

    Terms:

    A. Ergonomics

    B. Accessibility

    C. Usability

    Descriptions:

    1. The ability to serve the needs of as many people as possible - B
    1. the potential to accomplish user goals - C
    1. the ability to be used in a safe and comfortable way - A

1. Describe two devices, and two usability issues that can arise with each device (1.2.13):

    GPS system - small screen, low-quality speakers
    Tablets - can't handle accidental touch, small side buttons

### Chapter 2
1. Match the terms and the descriptions (2.1.1):

    Terms:

    A. arithmetic logic unit (ALU)

    B. central processing unit (CPU)

    C. memory data register (MDR)

    D. random access memory (RAM)

    E. control unit (CU)

    F. read only memory (ROM)

    G. memory address registrar (MAR)

    Descriptions:

    1. Stores executing program instructions, as well as any data that is needed. Can be overwritten - D
    1. Stores information to boot and operate the computer. Cannot be overwritten - F
    1. Can perform basic arithmetic, logical, or input/output operations. Interacts with primary memory. The "brain" of the computer system - B
    1. Controls retrieval of instructions from primary memory, as well as their sequence of execution, for the CPU - E
    1. Performs basic arithmetic, logical, or input/output operations. Does not directly communicate with primary memory - A
    1. Holds the memory address of the data to be used by the ALU - G
    1. Holds the data that is to be used by the ALU - C


1. Fill out the CPU block diagram (2.1.1):
![cpu block diagram](image15275.png)


1. Which is true of random access memory (2.1.5)?

    C
    
    A. It can be read by the CPU

    B. It is volitile
    
    C. Both A and B

    D. Neither A nor B

1. Which is true of read only memory (2.1.5)?

    A
    
    A. It can be read by the CPU

    B. It is volitile
    
    C. Both A and B

    D. Neither A nor B

1. Which is true of secondary memory (2.1.5)?

    D

    A. It can be read by the CPU

    B. It is volitile
    
    C. Both A and B

    D. Neither A nor B

1. Give two examples of secondary memory devices (2.1.5).

    Flash drives, DVDs
