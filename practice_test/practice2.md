# Practice Test \#2
## Jan 7, 2022

### Chapter 2

1. Give a brief description of each important function of an operating system (2.1.6):

    A. Peripheral Communication
  
    B. Memory Management
  
    C. Resource Monitoring and Multitasking
  
    D. Networking
  
    E. Disk Access and Data Mangement
  
    F. Security

1. What is the difference between a GUI and a CLI? Name two advantages of each (2.1.7).

1. Between a bit and a byte, which one is larger? How many of the smaller are in the larger (2.1.9)?

1. Most numbers we see are in base-10. What base is binary in? What base is hexadecimal in(2.1.9)?

1. Convert the following binary number to base-10 (2.1.9):

    > 1101

1. Convert the following base-10 number to binary (2.1.9):

    > 29

1. Name two character encoding schemes (2.1.10).

1. Construct a truth table for the following Boolean expression (2.1.11):

    > (a XOR b) AND NOT (c OR b)

### Chapter 3

1. Label  numbers 1-7 of the OSI model(3.1.1):

    ![OSI Model](osi.png)

1. What is the difference between a hub and a switch(3.1.1)?

1. What is the difference between LAN and WAN(3.1.1)?

1. What is the difference between lossy and lossless data compression (3.1.9)?