# Practice Test \#1
## Jan 6, 2022


### Lecture

1. Imagine two scenarios:
    
    * You have a linked list, with 4 items in it. You insert a new item at the beginning of the list.
    * You have an array of prototypes, with 4 items at the beginning of the array. You insert a new item at the beginning of the array.

    Which would you expect to be efficient, and which would you expect to be inefficient? Why?

1. Do a bitwise `xor` operation between these two binary numbers:

    * `11110000`
    * `10101010`


1. You can only create a hash for which of the following:
    * Mutable (changeable) data structures
    * Immutable (unchangeable) data structures

1. Hashes are used in which data structures (mark all that apply):
    * Linked Lists
    * Sets
    * Arrays
    * Hashmaps/Dictionaries

### Sorting algorithms

1. Which is the most efficient sorting algorithm of the three we learned?

    * MergeSort
    * BubbleSort
    * BogoSort

1. Which sorting algorithm involves comparing each item to nearby items, and making them step incrementally?

    * MergeSort
    * BubbleSort
    * BogoSort

1. Which sorting algorithm uses a "guess and check" method of sorting?

    * MergeSort
    * BubbleSort
    * BogoSort

1. Which sorting algorithm involves breaking the sequence down into smaller segments, and then recombining them?

    * MergeSort
    * BubbleSort
    * BogoSort

### Chapter 1

1. Fill out the components of the software lifecycle (1.1): 
    
    ![software lifecycle](image17.png)

1. Which of the following is SaaS? (1.1.4)
    * You pay a company so that you have access to a server, where you can host a database.
    * You pay a company to download and use their database management software.
    * You pay a company to access their online platform, which manages a database.

1. 

1. What is the difference between verification and validation? (1.1.7)

Todo: Changeover (1.1.5), Testing (1.1.7) 


1. Match the terms and the descriptions (1.2.1):

    Terms:

    A. Peripheral Device

    B. Computer Network

    C. Human Resource

    D. Hardware

    E. Software

    Descriptions:

    1. The physical elements of a computer (screen, CPU, etc)
    1. A series of instructions that can be understood by the CPU
    1. Any auxilliary device
    1. A set of computer systems that are interconnected and share resources
    1. People who can be useful
1. Match the terms and the descriptions (1.2.1):

    Terms:

    A. client

    B. Domain Name System Server
    
    C. router

    D. dumb terminal

    E. firewall

    F. email server

    G. client-server

    H. host

    I. thin client

    Descriptions:

    1. A device that only consists of a keyboard, a monitor, and a network card
    1. A relatively low performance terminal
    1. Receives data from the network
    1. Provides data or services to the network
    1. Allows users to access their email
    1. Networking device that accepts incoming data packets and distributes them across networks
    1. Links website names with network addresses, so that you don't have to directly type IP addresses into your browser
    1. Hardware or software that protects a network by limiting access
    1. An architecture system where clients request information and servers provide information

1. Match the terms and the descriptions (1.2.12):

    Terms:

    A. Ergonomics

    B. Accessibility

    C. Usability

    Descriptions:

    1. The ability to serve the needs of as many people as possible
    1. the potential to accomplish user goals
    1. the ability to be used in a safe and comfortable way

1. Describe two devices, and two usability issues that can arise with each device (1.2.13):

### Chapter 2
1. Match the terms and the descriptions (2.1.1):

    Terms:

    A. arithmetic logic unit (ALU)

    B. central processing unit (CPU)

    C. memory data register (MDR)

    D. random access memory (RAM)

    E. control unit (CU)

    F. read only memory (ROM)

    G. memory address registrar (MAR)

    Descriptions:

    1. Stores executing program instructions, as well as any data that is needed. Can be overwritten
    1. Stores information to boot and operate the computer. Cannot be overwritten
    1. Can perform basic arithmetic, logical, or input/output operations. Interacts with primary memory. The "brain" of the computer system
    1. Controls retrieval of instructions from primary memory, as well as their sequence of execution, for the CPU
    1. Performs basic arithmetic, logical, or input/output operations. Does not directly communicate with primary memory
    1. Holds the memory address of the data to be used by the ALU
    1. Holds the data that is to be used by the ALU


1. Fill out the CPU block diagram (2.1.1):
![cpu block diagram](image15275.png)


1. Which is true of random access memory (2.1.5)?
    
    A. It can be read by the CPU

    B. It is volitile
    
    C. Both A and B

    D. Neither A nor B

1. Which is true of read only memory (2.1.5)?
    
    A. It can be read by the CPU

    B. It is volitile
    
    C. Both A and B

    D. Neither A nor B

1. Which is true of secondary memory (2.1.5)?

    A. It can be read by the CPU

    B. It is volitile
    
    C. Both A and B

    D. Neither A nor B

1. Give two examples of secondary memory devices (2.1.5).