Practice Test 1:
* No questions involving hash
* I won't ask about the software lifecycle

Practice Test 2:
* I'll have questions about the OSI model, but you don't have to recreate it

Practice Test 3:
  * I won't ask about Byron or SPSS
  * I'll try to make these history questions easier
	You should know:

		Card input/output
		Transistors
		OS, loaders/linkers
		Spreadsheets/Word processors
		C++
		Java
		HTML
	
		Leibniz
		Pascal
		Turing
		Lovelace
