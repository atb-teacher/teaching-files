# Practice Test \#2 Answers

### Chapter 2

1. Give a brief description of each important function of an operating system (2.1.6):

    A. Peripheral Communication

    Communicates with hardware, such as keyboards, mice, printers, monitors.
  
    B. Memory Management

    Manage how apps use memory, and ensures that they don't interfere with each other

    C. Resource Monitoring and Multitasking

    Allows apps to use resources so that multiple apps can run at the same time
  
    D. Networking

    Manages connections to and interactions with networks
  
    E. Disk Access and Data Mangement

    Read to and write from memory stored on disks
  
    F. Security

    Provides security, mainly through requiring a user name and password

1. What is the difference between a GUI and a CLI? Name two advantages of each (2.1.7).

    GUI has a graphical interface, CLI has a command line interface.
    CLI -- easier to save and reuse commands, easier to automate
    GUI -- more user-friendly, you don't have to look as much stuff up

1. Between a bit and a byte, which one is larger? How many of the smaller are in the larger (2.1.9)?

    There are 8 bits in a byte

1. Most numbers we see are in base-10. What base is binary in? What base is hexadecimal in(2.1.9)?

    Binary is base 2, hexadecimal is base 16

1. Convert the following binary number to base-10 (2.1.9):

    > 1101

    (2^3) * 1 + (2^2) * 1 + (2^1) * 0 + (2^0) * 1
    
    8 + 4 + 0 + 1

    13

1. Convert the following base-10 number to binary (2.1.9):

    > 29

    We can subtract power of 2

    29 - 16 = 13
    13 - 8 = 5
    5 - 4 = 1
    1 - 1 = 0
    16 + 8 + 4 + 0 * 2 + 1
    (2^4) * 1 + (2^3) * 1 + (2^2) * 1 + (2^1) * 0 + (2^0) * 1
    11101
    
1. Name two character encoding schemes (2.1.10).

    ASCII and Unicode

1. Construct a truth table for the following Boolean expression (2.1.11):

    > (a XOR b) AND NOT (c OR b)

### Chapter 3

1. Label  numbers 1-7 of the OSI model(3.1.1):

    ![OSI Model](osi.png)

    You don't have to memorize this.

1. What is the difference between a hub and a switch(3.1.1)?

    A switch can identify which network device is connected to which port, a hub cannot

1. What is the difference between LAN and WAN(3.1.1)?

    Local Area Network -- a limited network separated from the internet
    Wider Area Network -- lets you connect to anywhere on the internet

1. What is the difference between lossy and lossless data compression (3.1.9)?

    Lossy involves compressing data in a way that leads you to lose information, loseless involves losing no info.