# Practice Test \#5

# 3.1.3

1. Which layers of the OSI model are associated with physical communication?

  A. Physical/data link
  B. Session/Presentation/Application
  C. Network/Transport

1. Which layers of the OSI model are associated with virtual communication?

  A. Physical/data link
  B. Session/Presentation/Application
  C. Network/Transport
