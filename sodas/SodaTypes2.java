public class SodaTypes2 {
	public static void main(String args[]) {
		System.out.println("Hello!");
		Soda mySoda = VendingMachine.Vend("Fanta");
		Soda mySoda2 = VendingMachine.Vend("Coke");
		Soda mySoda3 = VendingMachine.Vend("abc");
		mySoda.drink();
		mySoda2.drink();
		mySoda3.drink();
	}
}
	
abstract class Soda {
	String name;
	String flavor;
	boolean caffeinated;
	float sugar;
	public Soda( String nameParam, String flavorParam, boolean caffeinated, float sugar ) {
		this.name = nameParam;
		this.flavor = flavorParam;
		this.caffeinated = caffeinated;
		this.sugar = sugar;
	}
	public void drink() {
		System.out.println(
			String.format("Drinking %s, tastes like %s", this.name, this.flavor)
		);
	}
}

class Fanta extends Soda {
	public Fanta() {
		super("Fanta", "orange", false, 20);
	}
}

class Coke extends Soda {
	public Coke() {
		super("Coke", "cola-ey", true, 18);
	}
}

class Sprite extends Soda {
	public Sprite() {
		super("Sprite", "lemony", false, 17);
	}
}

class VendingMachine {
	public static Soda Vend( String sodaTypeParam ) {
		return switch (sodaTypeParam) {
			case "Fanta" -> new Fanta();
			case "Coke" -> new Coke();
			default -> new Sprite();
		};
	}
}
