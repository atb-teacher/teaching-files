public class SodaTypes3 {
	public static void main(String args[]) {
		Soda newSoda = Soda.startOfChain().build();
		newSoda.drink();
	}
}
	
class Soda {

	String name;
	String flavor;
	boolean caffeinated;
	float sugar;

	public static SodaBuilder startOfChain() {
		return new SodaBuilder();
	}
	public Soda( SodaBuilder builderParam ) {
		this.name = builderParam.name;
		this.flavor = builderParam.flavor;
		this.caffeinated = builderParam.caffeinated;
		this.sugar = builderParam.sugar;
	}

	static class SodaBuilder {
		String name = "Sprite";
		String flavor = "lemony";
		boolean caffeinated = false;
		float sugar = 17;

		public Soda build() {
			return new Soda( this );
		}
	}

	public void drink() {
		System.out.println(
			String.format("Drinking %s, tastes like %s", this.name, this.flavor)
		);
	}

}
