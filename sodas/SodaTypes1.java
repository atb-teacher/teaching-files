public class SodaTypes1 {
	public static void main(String args[]) {
		System.out.println("Hello!");
		Soda mySoda = new Fanta();
		mySoda.drink();
	}
}
	
abstract class Soda {
	String name;
	String flavor;
	boolean caffeinated;
	float sugar;
	public Soda( String nameParam, String flavorParam, boolean caffeinated, float sugar ) {
		this.name = nameParam;
		this.flavor = flavorParam;
		this.caffeinated = caffeinated;
		this.sugar = sugar;
	}
	public void drink() {
		System.out.println(
			String.format("Drinking %s, tastes like %s", this.name, this.flavor)
		);
	}
}

class Fanta extends Soda {
	public Fanta() {
		super("Fanta", "orange", false, 20);
	}
}
