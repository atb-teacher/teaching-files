public class SodaTypesBuilder {
	public static void main(String args[]) {
		Soda myFanta = Soda.startOfChain()
			.setName("Fanta")
			.setFlavor("orange")
			.setCaff(false)
			.build();
		Soda myCoke = Soda.startOfChain()
			.setName("Coke")
			.setFlavor("cola")
			.setCaff(true)
			.build();
		Soda mySprite = Soda.startOfChain()
			.build();
		myFanta.drink();
		myCoke.drink();
		mySprite.drink();
	}
}
	
class Soda {

	String name;
	String flavor;
	boolean caffeinated;
	float sugar;

	public static SodaBuilder startOfChain() {
		return new SodaBuilder();
	}
	public Soda( SodaBuilder builderParam ) {
		this.name = builderParam.name;
		this.flavor = builderParam.flavor;
		this.caffeinated = builderParam.caffeinated;
		this.sugar = builderParam.sugar;
	}

	static class SodaBuilder {
		String name = "Sprite";
		String flavor = "lemony";
		boolean caffeinated = false;
		float sugar = 17;

		public SodaBuilder setName(String inName) {
			this.name = inName;
			return this;
		}

		public SodaBuilder setFlavor(String inFlavor) {
			this.flavor = inFlavor;
			return this;
		}

		public SodaBuilder setCaff(boolean inCaff) {
			this.caffeinated = inCaff;
			return this;
		}

		public Soda build() {
			return new Soda( this );
		}
	}

	public void drink() {
		System.out.println(
			String.format("Drinking %s, tastes like %s", this.name, this.flavor)
		);
	}

}
