public class SodaTypesFactory {
	public static void main(String args[]) {
		Soda myFanta = VendingMachine.Vend("Fanta");
		Soda myCoke = VendingMachine.Vend("Coke");
		Soda mySprite = VendingMachine.Vend("abc");
		myFanta.drink();
		myCoke.drink();
		mySprite.drink();
	}
}
	
abstract class Soda {
	String name;
	String flavor;
	boolean caffeinated;
	float sugar;
	public Soda( String nameParam, String flavorParam, boolean caffeinated, float sugar ) {
		this.name = nameParam;
		this.flavor = flavorParam;
		this.caffeinated = caffeinated;
		this.sugar = sugar;
	}
	public void drink() {
		System.out.println(
			String.format("Drinking %s, tastes like %s", this.name, this.flavor)
		);
	}
}

class Fanta extends Soda {
	public Fanta() {
		super("Fanta", "orange", false, 20);
	}
}

class Coke extends Soda {
	public Coke() {
		super("Coke", "cola-ey", true, 18);
	}
}

class Sprite extends Soda {
	public Sprite() {
		super("Sprite", "lemony", false, 17);
	}
}

class VendingMachine {
	public static Soda Vend( String sodaTypeParam ) {
		return switch (sodaTypeParam) {
			case "Fanta" -> new Fanta();
			case "Coke" -> new Coke();
			default -> new Sprite();
		};
	}
}
