# Ordering System (from UML Diagram)
## A Java lab

We will create a `REPL` using the classes outlined in this UML Diagram (this is subject to minor changes).

![UML Diagram](uml2.png)

The `REPL` will start out by asking the user information to create a `NationalUser` or `InternationalUser`.

Then, you will allow that user to make an order. You will have five pre-made items that the user can choose from, and you will display them using the `Item.printInfo()` method. You will allow the user to add items to the order. We will assume that everyone wants to order at most one instance of each item.

When a user wants to add an item to the order, the `Order.addItem` method should run. It should add the item to the `itemArr` ArrayList.

After the order is completed, we will print out all the order's info with the `Order.printInfo()` method.
 
The `NatianalUser.checkZip()` method should return `true` iff the `zipCode` is five numbers.

Wherever it says `UUID`, you can use the `UUID` library to create a `Unique Universal Identifier` (a long string that's random).

```java
jshell> import java.util.UUID;

jshell> UUID.randomUUID().toString();
$2 ==> "458835e2-4bec-483c-ade5-e0887f4aced0"
jshell>
```

If you see `+attribute`, that is a public attribute. If you see `-attribute`, that is a private attribute. If you see `#attribute`, that is a protected attribute. If you disagree with the permissions I chose, let me know and I'll probably be alright with your changes.

If you stray from the UML diagram, document the changes and the reasons. Otherwise, I'll assume that you didn't understand.

Sample code [here](MyUML.java).
