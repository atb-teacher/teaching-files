# Binary Trees (not self-balancing) 

For this walkthrough/assignment, you will have to make a python file to go along with the assignment, and translate that python file into a java file at three different points.


 Let's start with a node to make a tree.

 ```python
class Node:
    def __init__(self, in_num: int):
        self.value = in_num
        self.left_child = None
        self.right_child = None
```

We can't directly run this file, but we can import it and use the `node` class. Here's what we can do with that class:

```python
>>> from BinTree import Node
>>> my_node = Node(5)
>>> print(my_node.value)
5
>>> print(my_node.left_child)
None
>>> print(my_node.right_child)
None
>>> my_node.left_child = Node(3)
>>> print(my_node.left_child)
<BinTree1.Node object at 0x7feb698f3d00>
>>> print(my_node.left_child.value)
3
```

We made something that looks like this (we don't have to create this representation in the program):

```
    Node(5)
    /     \
   /       \
  /         \
Node(3)     None
```
You can think of think of this as a little upside-down tree. The node on the top is called the `root node`.

Our tree will work better if it can represent itself in some way, so let's add a `print` method. 


```python
class Node:
     self.left_child = None
     self.right_child = None
 
    def print(self, level:int = 1, steps:str = "") -> None: # added line
        print(f"value {self.value} at level {level}") # added line
        print(f"after travelling {steps}") # added line
        if self.left_child is not None: # added line
            self.left_child.print( # added line
                level=level + 1, # added line
                steps=steps + "L", # added line
            ) # added line
        if self.right_child is not None: # added line
            self.right_child.print( # added line
                level=level + 1, # added line
                steps=steps + "R", # added line
            ) # added line
 
def demo(): # added line
    root = Node(5) # added line
    root.left_child = Node(3) # added line
    root.left_child.right_child = Node(4) # added line
    root.right_child = Node(7) # added line
    root.print() # added line
 
demo() # added line
```

Now our tree prints out this:

```bash
python3 BinTree.py 
value 5 at level 1
after travelling 
value 3 at level 2
after travelling L
value 4 at level 3
after travelling LR
value 7 at level 2
after travelling R
```

We can think of it looking like this (we don't have to create this representation in the program):

```
1           Node(5)
            /     \
           /       \
          /         \
2       Node(3)       Node(7)
       / \              /  \
      /   \            /    \
3  None   Node(4)   None   None
```


The recursive print method lets us see what is in the binary tree, and the different levels of the binary tree.


## At this point, make a java file

Make a java file called BinTree1.java. The class BinTree should only have a static method that corresponds to `demo` in the python example. The file should also have a class `Node` inside of it. After you've made `BinTree1.java`, continue.


Now let's create an `add` method so that we can add stuff to the root node.

```python
class Node:
    steps=steps + "R",
    )
	 
    def add(self, in_value:int) -> None: # added line
        if in_value < self.value: # added line
            if self.left_child is None: # added line
                self.left_child = Node(in_value) # added line
            else: # added line
                self.left_child.add(in_value) # added line
        elif in_value > self.value: # added line
            if self.right_child is None: # added line
                self.right_child = Node(in_value) # added line
            else: # added line
                self.right_child.add(in_value) # added line
def demo():
    root = Node(5)
    root.left_child = Node(3) # deleted line
    root.left_child.right_child = Node(4) # deleted line
    root.right_child = Node(7) # deleted line
    root.add(3) # added line
    root.add(4) # added line
    root.add(7) # added line
    root.print()
 
demo()
```

Instead of directly setting each node's children, we can use the `add` method, and we don't have to pay attention to the structure of the tree to add stuff.

Our tree can easily become unbalanced. To make our tree unbalanced, we can just give it the numbers `1` through `10` sequentially. Delete the `demo` function and replace it with this:

```python
def demo():
    root = Node(1)
    root.add(2)
    root.add(3)
    root.add(4)
    root.add(5)
    root.add(6)
    root.add(7)
    root.add(8)
    root.add(9)
    root.add(10)
    root.print()
```

This should print out the following:

```
python3 BinTree.py 
value 1 at level 1
after travelling 
value 2 at level 2
after travelling R
value 3 at level 3
after travelling RR
value 4 at level 4
after travelling RRR
value 5 at level 5
after travelling RRRR
value 6 at level 6
after travelling RRRRR
value 7 at level 7
after travelling RRRRRR
value 8 at level 8
after travelling RRRRRRR
value 9 at level 9
after travelling RRRRRRRR
value 10 at level 10
after travelling RRRRRRRRR
```

We have tons of stuff on the right side of our binary tree, but the left side is bare!

## At this point, make a java file

Make a file called `BinTree2.java` that prints out a poorly-balanced binary tree, like the one shown above, and then continue.

Now let's add a method to print out the values in the tree in order, from lowest to highest:

```python
class Node:

    def in_order(self) -> None: # added line
        if self.left_child is not None: # added line
            self.left_child.in_order() # added line
        print(self.value) # added line
        if self.right_child is not None: # added line
            self.right_child.in_order() # added line

```

Delete the `demo` function and replace it with this (for better balance, and to use our new method):

```python
def demo():
    root = Node(5)
    root.add(3)
    root.add(2)
    root.add(4)
    root.add(7)
    root.add(6)
    root.add(9)
    root.add(8)
    root.add(10)
    root.in_order()
```

## At this point, make a java file

Make a file called `BinTree3.java` that prints out info in order.
