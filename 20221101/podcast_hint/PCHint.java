import java.io.FileReader;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.ArrayList;

public class PCHint {
	public static void main(String args[]) {
		Pattern namePattern = Pattern.compile("begin-name(?<name>.*?)end-name");
		Pattern agePattern = Pattern.compile("begin-age(?<age>.*?)end-age");
		ArrayList<String> names = new ArrayList<String>();
		ArrayList<String> ages = new ArrayList<String>();
		try {
			FileReader fin = new FileReader("data.txt");
			Scanner src = new Scanner(fin);
			String activeLine;
			Matcher nameMatcher;
			Matcher ageMatcher;
			while (src.hasNext()) {
				activeLine = src.nextLine();
				nameMatcher = namePattern.matcher(activeLine);
				ageMatcher = agePattern.matcher(activeLine);
				try {
					nameMatcher.find();
					System.out.println(nameMatcher.group("name"));
					names.add(nameMatcher.group("name").trim());
				} catch(Exception e) {
					System.out.println(e);
				}
				try {
					ageMatcher.find();
					System.out.println(ageMatcher.group("age"));
					ages.add(ageMatcher.group("age").trim());
				} catch(Exception e) {
					System.out.println(e);
				}
			}
		} catch(Exception e) {
			System.out.println(e);
		}
		for (int i=0; i<names.size(); i++) {
			System.out.println(
				String.format(
					"%x) %s is %s years old",
					i,
					names.get(i),
					ages.get(i)
				)
			);
		}	
	}
}
