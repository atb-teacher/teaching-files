# HTML Notes Combined, parts 1 and 2

You will do this two times, once for `HTML Notes Part 1`, and once for `HTML Notes Part 2`.

> Look at the html file you made, and the html file your groupmates made, and combine them into one file. Minimum 150 words, but can be larger.

Here are the group sections:

```
Part 1
1.1.1, 1.1.2
1.2.1
```

* TJ, Tanush, Kieran

-----

```
Part 2
1.1.3, 1.1.4
1.2.3
```

* Sofronis, Kyle, Reid 
-----

```
Part 3
1.1.5, 1.1.6
1.2.4, 1.2.5
```

* Carter, Braden, Lucas
-----


```
Part 4
1.1.7
1.2.6
```

* Declan, Nicholas, Aila
-----


```
Part 5
1.1.8, 1.1.9
1.2.7 pg. 34-36
```

* Matthew, Siena, Nathaniel
-----


```
Part 6
1.1.10, 1.1.11
1.2.7 pg. 37-39
```

* Jacob, Farzaan, Isaac
-----


```
Part 7
1.1.12, 1.1.13, 1.1.14
1.2.8, 1.2.9
```

* Thalen, Dillon, Ethan
-----

