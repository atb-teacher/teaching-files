# Pokemon Lab
### A Java Assignment

Make a `REPL` (read evaluate print loop) that allows the user to choose one of three starter pokemon, and fight randomly generated pokemon. Pokemon should level up after fighting enough. Your program should have:

* A Pokemon class with
  * HP
  * Attack
  * Type
  * Level
  * Experience
  * A "level up" method

* 3 classes, one for each starter pokemon
  * One fire type
  * One water type
  * One leaf type

Fire type is strong against leaf type.
Leaf type is strong against water type.
Water type is strong against fire type.

