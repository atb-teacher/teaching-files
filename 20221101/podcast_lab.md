# Podcast Episode Viewer
### A Java Assignment

Create a program that takes an rss feed as an argument, and lists the first five episodes of the podcast. When the user chooses an episode, show more details for that episode.

```
java RssViewer "https://feeds.npr.org/510282/podcast.xml" # NPR's Pop Culture podcast
fetching episodes...

1) Russian Doll
2) Grace and Frankie
3) Ozark
4) Titanic
5) Summer Movie Guide

What would you like to know more about?
: 5

Summary: It's about to be summer. While that means sunshine and maybe even a chance to relax, it also means summer entertainment. Whether you're ready to venture out into theaters or you're still enjoying the comforts of your couch, we are here to talk about some of the best TV, film and music that's coming straight to you. Vote for your favorite American Idol contestants at npr.org/AmericanIdol
```

You can start with [this file](HttpTestMain.java), which runs on replit.com. You can simply copy the file's contents and paste it into `Main.java`, and then hit the big green button.

[This file](HttpTest.java) is very similar to the previous file. To run this file on replit.com, make a file with the same name as the class, and run the commands `javac HttpTest.java` and `java HttpTest`.

### Extra credit +1 point

Extra credit for moving all inner html tags, such as `<bold>`, `<span>`, `<em>`, etc.
