import java.io.FileReader;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class FileParser {
	public static void main(String args[]) {
		Pattern studentPattern = Pattern.compile("^(?<firstName>[A-Za-z]+)\\s(?<lastName>[A-Za-z]+)\\s+(?<age>\\d+)\\s+(?<cards>\\d+)$");
		try {
			FileReader fin = new FileReader("data.txt");
			Scanner src = new Scanner(fin);
			String activeLine;
			Matcher studentMatcher;
			src.nextLine();
			while (src.hasNext()) {
				activeLine = src.nextLine();
				studentMatcher = studentPattern.matcher(activeLine);
				studentMatcher.find();
				System.out.print(studentMatcher.group("firstName")); // added line
				System.out.print(studentMatcher.group("lastName")); // added line
				System.out.print(studentMatcher.group("age")); // added line
				System.out.println(studentMatcher.group("cards")); // added line
			}
		} catch(Exception e) {
			System.out.println(e);
		}
	}
}

