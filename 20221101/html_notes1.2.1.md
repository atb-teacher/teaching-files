# HTML Notes part 2

Take notes on a section of the book, in an HTML file. Your notes should combine to be a minimum of 200 words, or 150 words if you have use a list element. Word count should not include html code.

Turn in a file with the given filename, or a link to a replit that has the given filename as a project name. In replit, we'll keep the main file named `index.html`. You should not work with other students who have your section (that will come later).

Here are the sections of the book you should take notes on:


```
Part 1
1.2.1
```

* TJ, Tanush, Kieran

-----

```
Part 2
1.2.3
```

* Sofronis, Kyle, Reid 
-----

```
Part 3
1.2.4, 1.2.5
```

* Carter, Braden, Lucas
-----


```
Part 4
1.2.6
```

* Declan, Nicholas, Aila
-----


```
Part 5
1.2.7 pg. 34-36
```

* Matthew, Siena, Nathaniel
-----


```
Part 6
1.2.7 pg. 37-39
```

* Jacob, Farzaan, Isaac
-----


```
Part 7
1.2.8, 1.2.9
```

* Thalen, Dillon, Ethan
-----

