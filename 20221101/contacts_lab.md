# Contacts Lab
## A Java lab

Make a program in Java that can load the provided dataset [data_to_parse.txt](data_to_parse.txt), and create an array of `Contact`s. `Contact` will be a class you create to store a specific contact's name, age, and address.

You will be provided a text file full of names, ages, and addresses. You should use regex to parse the files. Learn more about regex by going to [regexone](https://regexone.com/) and [regex101](https://regex101.com/), and then following the [python guide](regex_parse_file/parse_file_python.md) and then the [java guide](regex_parse_file/parse_file_java.md) to parse a file with regex.

The user should be presented with a `REPL` interface that allows them to sort the `Contact`s by name, using bubble sort. They should also be able to perform all `CRUD` operations: they should be able to (c)reate a contact, (r)etrieve information on a contact based on their name, (u)pdate a contact by changing their name, age, or address, or (d)elete a contact.

## +1 Extra Credit

Allow the user to export the contact as a CSV file.

