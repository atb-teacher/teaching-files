public class Soda2 {
	public static void main(String[] args) {
		// the vending machine is a factory that makes sodas
		Soda mySoda = VendingMachine.vend("coke");
		mySoda.describe();
		Soda mySoda2 = VendingMachine.vend("fanta");
		mySoda2.describe();
	}
}

class Soda {
	String flavor;
	int sugar;
	boolean caffeine;
	public Soda(String flavorParam, int sugarParam, boolean caffeineParam) {
		flavor = flavorParam;
		sugar = sugarParam;
		caffeine = caffeineParam;
	}
	public void describe()  {
		System.out.println(
			String.format(
				"%s flavored soda with %d grams of suggar, caf = %b.",
				flavor,
				sugar,
				caffeine
			)
		);
	}
}

class Coke extends Soda {
	public Coke() {
		super("cola", 18, true); // 
	}
}

class Fanta extends Soda {
	public Fanta() {
		super("orange", 22, false); // 
	}
}

class VendingMachine { // factory
	public static Soda vend( String sodaTypeParam ) {
		return switch( sodaTypeParam.toLowerCase() ) {
			case "coke" -> new Coke();
			case "fanta" -> new Fanta();
			default -> new Soda("water", 0, false);
		};
	}
}
