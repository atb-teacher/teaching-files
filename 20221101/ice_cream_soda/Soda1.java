public class Soda1 {
	public static void main(String[] args) {
		Soda mySoda = new Coke();
		mySoda.describe();
	}
}

class Soda {
	String flavor;
	int sugar;
	boolean caffeine;
	public Soda(String flavorParam, int sugarParam, boolean caffeineParam) {
		flavor = flavorParam;
		sugar = sugarParam;
		caffeine = caffeineParam;
	}
	public void describe()  {
		System.out.println(
			String.format(
				"%s flavored soda with %d grams of suggar, caf = %b.",
				flavor,
				sugar,
				caffeine
			)
		);
	}
}

class Coke extends Soda {
	public Coke() {
		super("cola", 18, true); // 
	}
}
