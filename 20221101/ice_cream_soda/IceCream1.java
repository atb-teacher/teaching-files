public class IceCream1 {
	public static void main(String[] args) {
		IceCream lucasIceCream = new IceCream("strawberry", "bowl", "gummi bears");
		lucasIceCream.describe();
	}
}

class IceCream {
	String flavor;
	String topping;
	String base;
	public IceCream(String flavorParam, String toppingParam, String baseParam) {
		flavor = flavorParam;
		topping = toppingParam;
		base = baseParam;
	}
	public void describe() {
		System.out.println(
			String.format(
				"%s ice cream on a %s with %s.",
				flavor,
				base,
				topping
			)
		);
	}
}
