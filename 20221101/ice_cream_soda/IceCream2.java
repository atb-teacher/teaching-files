public class IceCream2 {
	public static void main(String[] args) {
		IceCream lucasIceCream = new IceCream("strawberry", "bowl", "gummi bears");
		lucasIceCream.describe();
		IceCreamBuilder myBuilder = new IceCreamBuilder(); // construct a new builder
					
		// Sof asks for mocha ice cream with choco chips
		IceCream sofIceCream = myBuilder.setFlavor("mocha").setTopping("choco chips").build();
		sofIceCream.describe();

		// Carter asks for chocolate ice cream, he gets choco chips he didn't ask for
		IceCream carterIceCream = myBuilder.setFlavor("chocolate").build();
		carterIceCream.describe();
	}
}

class IceCream {
	String flavor;
	String topping;
	String base;
	public IceCream(String flavorParam, String toppingParam, String baseParam) {
		flavor = flavorParam;
		topping = toppingParam;
		base = baseParam;
	}
	public void describe() {
		System.out.println(
			String.format(
				"%s ice cream on a %s with %s.",
				flavor,
				base,
				topping
			)
		);
	}
}

class IceCreamBuilder {
	String flavor = "vanilla";
	String topping = "sprinkles";
	String base = "cone";

	public IceCream build() {
		return new IceCream(flavor, topping, base);
	}

	public IceCreamBuilder setFlavor(String flavorParam) {
		flavor = flavorParam;
		return this;
	}

	public IceCreamBuilder setTopping(String toppingParam) {
		topping = toppingParam;
		return this;
	}
}
