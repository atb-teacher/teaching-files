public class IceCream3 {
	public static void main(String[] args) {

		IceCream lucasIceCream = new IceCream("strawberry", "bowl", "gummi bears");
		lucasIceCream.describe();

		IceCream sofIceCream = IceCream.builder()
			.setFlavor("mocha")
			.setTopping("choco chips")
			.build();
		sofIceCream.describe();

		IceCream carterIceCream = IceCream.builder()
			.setFlavor("chocolate")
			.build();
		carterIceCream.describe();
	}
}

class IceCream {
	public static IceCreamBuilder builder() {
		return new IceCreamBuilder();
	}

	static class IceCreamBuilder {
		String flavor = "vanilla";
		String topping = "sprinkles";
		String base = "cone";
	
		public IceCream build() {
			return new IceCream(flavor, topping, base);
		}
	
		public IceCreamBuilder setFlavor(String flavorParam) {
			flavor = flavorParam;
			return this;
		}
	
		public IceCreamBuilder setTopping(String toppingParam) {
			topping = toppingParam;
			return this;
		}
	}

	String flavor;
	String topping;
	String base;
	public IceCream(String flavorParam, String toppingParam, String baseParam) {
		flavor = flavorParam;
		topping = toppingParam;
		base = baseParam;
	}
	public void describe() {
		System.out.println(
			String.format(
				"%s ice cream on a %s with %s.",
				flavor,
				base,
				topping
			)
		);
	}
}

