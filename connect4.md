# Connect Four
#### A Java Assignment
#### Can only be turned in as a file

Create a REPL (Read Evaluate Print Loop) to play Connect Four. You should have a class Board, which has a 2D array or ArrayList to keep track of the state of the game (to represent 7 columns and 6 rows), and a class Token, which can either be an `X` or an `O`.

The game will assume that users are taking turns, and will ask each user to choose a column. The Board class will have a dropToken method, as well as a checkWinner method and a checkTie method.

After each move, the Board class should run a boardPrint method, which will print out something like this:

```
.......
.......
.......
.......
..X....
.XOO...
```

Example for recursive check [https://gitlab.com/atb-teacher/teaching-files/-/tree/main/counterExample](https://gitlab.com/atb-teacher/teaching-files/-/tree/main/counterExample)

#### Extra Credit (+1)
Implement a recursive algorithm that counts down from 4, steps in a direction, and checks if 4 Tokens are the same type in that direction. Use this recursive algorithm to check if there's a winner.

#### Extra Credit (+1)
Let the user choose the size of the board
