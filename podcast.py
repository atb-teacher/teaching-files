import requests

import re

podcast_rss = str(requests.get("https://feeds.npr.org/510282/podcast.xml").content)

matches = re.findall(r'<title>(.*?)</title>', podcast_rss)

matches_filtered = []

matches_index = 0

while len(matches_filtered) < 5 and matches_index < len(matches):
    if matches[matches_index] != "Pop Culture Happy Hour":
        matches_filtered.append(matches[matches_index])

    matches_index += 1

print(matches_filtered)
    

