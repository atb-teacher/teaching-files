import java.util.Scanner;

public class CheckScantron {

	public static void main(String[] args) {
		Scanner myObj = new Scanner(System.in);
		char[] answers = {'A', 'E', 'B', 'B', 'C'};
		char[] studentGuesses = {'B', 'C', 'D', 'B', 'C'};
		int correctAnswers = 0;
		for (int i=0; i<answers.length; i++) {
			if (answers[i] == studentGuesses[i]) {
				correctAnswers++;
			}
		}
		System.out.print("Correct answers: ");
		System.out.println(correctAnswers);
	}
}
