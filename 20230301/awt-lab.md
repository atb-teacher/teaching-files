# Simple AWT Lab
#### A Java Assignment

Make a GUI Java app with Abstract Window Toolkit (or `AWT`). The app should have an area where the user can type in a number, and a "submit" button. Then the app should tell the user if the number they submitted is even or odd. You can tell the user by setting the text of a label.

## AWT

Here's a simple `AWT` program.

```java
import java.awt.Frame;
import java.awt.Label;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.FlowLayout;

public class Hello {

    public static void main(String[] args) {
        Frame f=new Frame("Hello World example of awt application"); // (1)
        Label label1=new Label("Hello World", Label.CENTER); // (2)
        f.add(label1); // (3)

        f.setSize(300,100);
        f.setVisible(true);
        f.setLayout(new FlowLayout()); // (4)
        f.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent event) { // (5)
                System.exit(0);
            }
        });
    }
}
```

1. A `frame` is high up on the hierarchy (seen below)
1. A label is a component. Other components can be seen [here](https://docs.oracle.com/javase/7/docs/api/java/awt/Component.html#:~:text=Direct%20Known%20Subclasses%3A)
1. Adding the component to the frame
1. This controls how the components are automatically arranged within the window. Example [here](layouts.gif)
1. This is an anonymous class. You create an instance of `WindowAdapter`, but you customize what the method `windowClosing` will do. More about anonymous classes [here](https://docs.oracle.com/javase/tutorial/java/javaOO/anonymousclasses.html)

### AWT Hierarchy

![Hierarchy here](java-awt.png)

When you program with `AWT` you should mostly visualize yourself creating a frame and putting a bunch of components in that frame.
