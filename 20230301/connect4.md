# Connect Four
#### A Java Assignment
#### Can only be turned in as a file

Create a REPL (Read Evaluate Print Loop) to play Connect Four. You should have a class Board, which has a 2D array or ArrayList to keep track of the state of the game (to represent 7 columns and 6 rows), and a class Token, which can either be an `X` or an `O`.

The game will assume that users are taking turns, and will ask each user to choose a column. The Board class will have a dropToken method, as well as a checkWinner method and a checkTie method. A game is not considered a tie until the board is full.

After each move, the Board class should run a boardPrint method, which will print out something like this:

```
.......
.......
.......
.......
..X....
.XOO...
```

#### Extra Credit (+1)
Implement a recursive algorithm that counts down from 4, steps in a direction, and checks if 4 Tokens are the same type in that direction. Use this recursive algorithm to check if there's a winner.

For a hint, look at this [tic tac toe guide](TicTacToeGuide.md).

#### Extra Credit (+1)
Let the user choose the size of the board
