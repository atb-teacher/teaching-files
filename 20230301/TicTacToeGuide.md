# Tic Tac Toe Guide

Let's start by making a Java file that will randomly create Tic Tac Toe boards.

```java
import java.util.Random;

public class TicTacToeChecker {
    public static void main(String args[]) {
		char[][] board = makeBoard(3);
		printBoard(board);
    }

	public static char[][] makeBoard(int boardSize) {
		Random activeRand = new Random();
		char[][] board = new char[boardSize][boardSize];
		for (int i=0; i<boardSize; i++) {
			for (int j=0; j<boardSize; j++) {
				board[i][j] = activeRand.nextBoolean() ? 'x' : 'o';
			}
		}
		return board;
	}

	public static void printBoard(char[][] inBoard) {
		// assumes inner arrays have consistent length
		for (int i=0; i<inBoard.length; i++) {
			for (int j=0; j<inBoard[0].length; j++) {
				System.out.print(inBoard[i][j]);
			}
			System.out.println();
		}
	}
}
```

The line `board[i][j] = activeRand.nextBoolean() ? 'x' : 'o'` is just an in-line if conditional. It asks what our random generator genorates (either `true` or `false`), and places an `x` or `o` accordingly.

In our print method, we strategically use `System.out.print` and `System.out.println` so that we only print a new line after printing a row.

Now, let's make a `Checker` class to check our board. We will get an error as we do this.

```java
public static void main(String args[]) {
 		char[][] board = makeBoard(3);
 		printBoard(board);
		Checker activeChecker = new Checker(0, 0, 1, 1); // added line
     }
``` 

```java
class Checker { // added line

	// main board // added line
	static char[][] board; // added line
	// the number of matches needed to win // added line
	static int winningNum; // added line

	// these change when we create a new checker // added line
	final int startX; // added line
	final int startY; // added line
	final int deltaX; // added line
	final int deltaY; // added line
	int positionX; // added line
	int positionY; // added line

	// found type we're checking against // added line
	final char tokenType; // added line

	public Checker(int posX, int posY, int delX, int delY) { // added line
		positionX = posX; // added line
		positionY = posY; // added line
		startX = posX; // added line
		startY = posY; // added line
		deltaX = delX; // added line
		deltaY = delY; // added line
		tokenType = getToken(); // added line
	} // added line

	private char getToken() { // added line
		return board[positionY][positionX]; // added line
	} // added line
```

Our `Checker` class has a couple static variables. We can make a bunch of `Checker`s that will all share one `board` and one `winningNum`. Given a certain board and a certain required matches to win (usually 3-in-a-row wins the game), we can make a bunch of `Checker`s. Each `Checker` will have a set starting position, and a set directional change of x and y coordinates, and it will set the token it's checking against as the first token it sees. All these are final. The position we are looking at changes as we look, so that is not final.

The way we set it up, we will get an error if we create a `Checker` before we set the board.

We can make methods to allow ourselves to setup the board and the required matches, which will fix our error.

```java
public class TicTacToeChecker {
    public static void main(String args[]) {
 		char[][] board = makeBoard(3);
 		printBoard(board);
		Checker.setBoard(board); // added line
		Checker.setWinNum(3); // added line
 		Checker activeChecker = new Checker(0, 0, 1, 1);
    }
```

```java
 		return board[positionY][positionX];
 	}
 
	public static void setBoard(char[][] inBoard) { // added line
		board = inBoard; // added line
	} // added line

	public static void setWinNum(int inWinningNum) { // added line
		winningNum = inWinningNum; // added line
	} // added line

}
```

I don't like the way that `new Checker(0, 0, 1, 1)` looks, so I want to make a factory:

```java
 		printBoard(board);
 		Checker.setBoard(board);
 		Checker.setWinNum(3);
		Checker activeChecker = new Checker(0, 0, 1, 1); // deleted line
		CheckerFactory checkMaker = new CheckerFactory(); // added line
		Checker activeChecker = checkMaker.posX(0).posY(0).delX(1).delY(1).makeChecker(); // added line
     }
```

```java
class CheckerFactory { // added line
	int positionX; // added line
	int positionY; // added line
	int deltaX; // added line
	int deltaY; // added line
	 // added line
	public CheckerFactory posX(int inX) { // added line
		positionX = inX; // added line
		return this; // added line
	} // added line
	public CheckerFactory posY(int inY) { // added line
		positionY = inY; // added line
		return this; // added line
	} // added line
	public CheckerFactory delX(int inX) { // added line
		deltaX = inX; // added line
		return this; // added line
	} // added line
	public CheckerFactory delY(int inY) { // added line
		deltaY = inY; // added line
		return this; // added line
	} // added line
	public Checker makeChecker() { // added line
		return new Checker(positionX, positionY, deltaX, deltaY); // added line
	} // added line
} // added line
```

Now we can make the actual check method.

```java
public class TicTacToeChecker {
 		Checker.setWinNum(3);
 		CheckerFactory checkMaker = new CheckerFactory();
 		Checker activeChecker = checkMaker.posX(0).posY(0).delX(1).delY(1).makeChecker();
		activeChecker.checkArray(); // added line
     }
```
 
```java
class Checker {
 		winningNum = inWinningNum;
 	}
 
	private void incrementPos() { // added line
		positionX += deltaX; // added line
		positionY += deltaY; // added line
	} // added line

	public void printStatus() { // added line
		System.out.println( // added line
			String.format( // added line
				"start x: %d\n" + // added line
				"start y: %d\n" + // added line
				"change x: %d\n" + // added line
				"change y: %d\n", // added line
				startX, startY, deltaX, deltaY // added line
			) // added line
		); // added line
	} // added line

	public boolean checkArray() { // added line
		boolean foundWinPart = true; // added line
		for (int i=1; i<winningNum; i++) { // added line
			incrementPos(); // added line
			if (getToken() != tokenType) { // added line
				foundWinPart = false; // added line
				break; // added line
			} // added line
		} // added line
		if (foundWinPart) { // added line
			printStatus(); // added line
		} // added line
		return foundWinPart; // added line
	} // added line

}
```
In the `checkArray` method we step through the board using our `incrementPos` method, and we get the token at the current position with our `getToken` method we wrote a bit ago. `checkArray` uses a boolean flag to check if you have found a token that doesn't match the 1st token.

You should be able to spam this program until there's a diagonal solution from topleft to bottomright, and the solution should print out.

Now let's check for more solutions:
```java
public class TicTacToeChecker {
 		Checker.setBoard(board);
 		Checker.setWinNum(3);
 		CheckerFactory checkMaker = new CheckerFactory();
		Checker activeChecker = checkMaker.posX(0).posY(0).delX(1).delY(1).makeChecker(); // deleted line
		Checker activeChecker; // added line
		// check columns // added line
		for (int i=0; i<3; i++) { // added line
			activeChecker = checkMaker.posX(i).posY(0).delX(0).delY(1).makeChecker(); // added line
			activeChecker.checkArray(); // added line
		} // added line
		// check rows // added line
		for (int i=0; i<3; i++) { // added line
			activeChecker = checkMaker.posX(0).posY(i).delX(1).delY(0).makeChecker(); // added line
			activeChecker.checkArray(); // added line
		} // added line
		// check diags // added line
		activeChecker = checkMaker.posX(0).posY(0).delX(1).delY(1).makeChecker(); // added line
		activeChecker.checkArray(); // added line
		activeChecker = new Checker(2, 0, -1, 1); // added line
		activeChecker = checkMaker.posX(2).posY(0).delX(-1).delY(1).makeChecker(); // added line
 		activeChecker.checkArray();
     }
```

We loop over the rows and columns to check them. This solution should work with a 3x3 board.

It gets trickier if the board is larger than the number of matches required to win. With the final version, I forgot to use my factory while checking diagonals, and I should probably put the row/column/diagonal checks into their own methods.

```java
 
public class TicTacToeChecker {
    public static void main(String args[]) {
		char[][] board = makeBoard(3); // deleted line
		char[][] board = makeBoard(4); // added line
 		printBoard(board);
		int winNum = 3; // added line
 		Checker.setBoard(board);
 		Checker.setWinNum(3);
 		CheckerFactory checkMaker = new CheckerFactory();
 		Checker activeChecker;

 		// check columns
		for (int i=0; i<3; i++) { // deleted line
			activeChecker = checkMaker.posX(i).posY(0).delX(0).delY(1).makeChecker(); // deleted line
			activeChecker.checkArray(); // deleted line
		for (int i=0; i<board[0].length; i++) { // added line
			for (int j=0; j<=(board[0].length - winNum); j++) { // added line
				activeChecker = checkMaker.posX(i).posY(j).delX(0).delY(1).makeChecker(); // added line
				activeChecker.checkArray(); // added line
			} // added line
 		}

 		// check rows
		for (int i=0; i<3; i++) { // deleted line
			activeChecker = checkMaker.posX(0).posY(i).delX(1).delY(0).makeChecker(); // deleted line
			activeChecker.checkArray(); // deleted line
		for (int i=0; i<board.length; i++) { // added line
			for (int j=0; j<=(board.length - winNum); j++) { // added line
				activeChecker = checkMaker.posX(j).posY(i).delX(1).delY(0).makeChecker(); // added line
				activeChecker.checkArray(); // added line
			} // added line
 		}
 		// check diags
		activeChecker = checkMaker.posX(0).posY(0).delX(1).delY(1).makeChecker(); // deleted line
		activeChecker.checkArray(); // deleted line
		activeChecker = new Checker(2, 0, -1, 1); // deleted line
		activeChecker = checkMaker.posX(2).posY(0).delX(-1).delY(1).makeChecker(); // deleted line
		activeChecker.checkArray(); // deleted line
		for (int i=0; i<=board[0].length - winNum; i++) { // added line
			for (int j=0; j<=board.length - winNum; j++) { // added line
				activeChecker = new Checker(i, j, 1, 1); // added line
				activeChecker.checkArray(); // added line
				activeChecker = new Checker(board[0].length - i - 1, j, -1, 1); // added line
				activeChecker.checkArray(); // added line
			} // added line
		} // added line

    }

```
