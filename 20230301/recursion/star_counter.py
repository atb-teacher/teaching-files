import random
sky = []
for i in range(10):
    row = []
    for j in range(10):
        row.append(random.choice("....*"))
    sky.append(row)

for i in range(len(sky)):
    print(
        "".join((space for space in sky[i]))
    )
            
def count_stars(x, y, sky):
    # if too big
    if x >= len(sky[y]):
        y += 1
        # if too big
        if y >= len(sky):
            print("done")
        else:
            count_stars(0, y, sky)
    else:
        if sky[y][x] == "*":
            print(f"Star at x={x}, y={y}")
        count_stars(x + 1, y, sky)
    
count_stars(0, 0, sky)
