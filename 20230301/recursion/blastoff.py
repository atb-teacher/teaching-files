import time

def blastoff(in_num:int) -> None:
    if in_num <= 0:
        print("Blastoff!")
        blastoff(in_num - 1)
    else:
        print(f"{in_num}...")
        time.sleep(0.3)
        blastoff(in_num - 1)

blastoff(5)
