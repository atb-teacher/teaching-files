class Blastoff {
	int counter;
	Blastoff(int counterParam) {
		counter = counterParam;
	}

	public void go() {
		if (counter <= 0) {
			System.out.println(
				"Blastoff!"
			);
		}
		else {
			System.out.println(
				String.format(
					"%d...",
					counter
				)
			);
			counter--;
			go();
		}
	}
	
	public static void staticGo(int staticCounter) {
		if (staticCounter <= 0) {
			System.out.println(
				"Blastoff!"
			);
		}
		else {
			System.out.println(
				String.format(
					"%d...",
					staticCounter	
				)
			);
			staticGo(staticCounter - 1);
		}
	}

	public static void main(String[] args) {
		Blastoff.staticGo(5);
		Blastoff myBlastoff = new Blastoff(3);
		myBlastoff.go();
	}
}
