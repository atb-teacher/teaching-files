# Sort Cards with Bogosort

Let's start by making a card class to represent Pokemon cards, that have a name and a price. We will also make a Main class to run.


    class Card {
    	public String name;
    	public double price;
    	public Card(String inName, double inPrice) {
    		this.name = inName;
    		this.price = inPrice;
    	}
    }

    public class Main {
    }


We will be using ArrayLists, so we need to import:

	import java.util.ArrayList; // added line
	 class Card {

And we can create an ArrayList of cards.
	 
	 public class Main {
		public static void main(String args[]) { // added line
			double[] prices = {1.93, 1.87, 3_012.12}; // added line
			String[] names = {"Charizard", "Pikachu", "Magicarp"}; // added line
			ArrayList<Card> cardArr = new ArrayList<Card>(); // added line
			for (int i=0; i < prices.length; i++) { // added line
				cardArr.add(new Card( names[i], prices[i] )); // added line
				System.out.print(cardArr.get(i).name); // added line
				System.out.print(cardArr.get(i).price); // added line
			} // added line
		} // added line
	}

This should print out some card information, but we can create a cleaner print function:

	 public class Main {
		public static void printCardArr(ArrayList<Card> inArr) { // added line
			for (int i=0; i < inArr.size(); i++) { // added line
				System.out.print(inArr.get(i).name); // added line
				System.out.print(" - "); // added line
				System.out.print(inArr.get(i).price); // added line
				System.out.print("; "); // added line
			} // added line
			System.out.println(); // added line
		} // added line
	 	public static void main(String args[]) {
	 		double[] prices = {1.93, 1.87, 3_012.12};
	 		String[] names = {"Charizard", "Pikachu", "Magicarp"};

And use our print function:

	 		ArrayList<Card> cardArr = new ArrayList<Card>();
	 		for (int i=0; i < prices.length; i++) {
	 			cardArr.add(new Card( names[i], prices[i] ));
				System.out.print(cardArr.get(i).name); // deleted line
				System.out.print(cardArr.get(i).price); // deleted line
	 		}
			printCardArr(cardArr); // added line
	 	}
	 }

To shuffle our ArrayList, we can use a process where we swap every index with another random index. Let's start by importing a random number generator:

	import java.util.Random; // added line
	import java.util.ArrayList;
	class Card {
		public String name;

And we can make our method to shuffle the cards:

public class Main {
		public static Random rand = new Random(); // added line
		public static ArrayList<Card> shuffleArray(ArrayList<Card> inArr) { // added line
			int randInt; // added line
			Card swapCard; // added line
			for (int i=0; i<inArr.size(); i++) { // added line
				randInt = Main.rand.nextInt(inArr.size()); // added line
				swapCard = inArr.get(randInt); // added line
				inArr.set(randInt, inArr.get(i)); // added line
				inArr.set(i, swapCard); // added line
			} // added line
			return inArr; // added line
		} // added line

We will want to sort our array both by name and price. So, in each case, we can shuffle our array and then check if it is sorted. Let's write a method that can check if our array is sorted by price, and we can put that method right after the `shuffleArray` method we just made. With this, we should have enough to bogosort the array by value, because we will have both the "guess" and "check" portion.


	 		}
	 		return inArr;
	 	}
		public static boolean checkSortedValue(ArrayList<Card> inArr) { // added line
			for (int i=0; i<inArr.size() - 1; i++) { // added line
				if (inArr.get(i).price > inArr.get(i+1).price) { // added line
					return false; // added line
				} // added line
			} // added line
			return true; // added line
		} // added line
		public static ArrayList<Card> bogoSortValue(ArrayList<Card> inArr) { // added line
			while (!checkSortedValue(inArr)) { // added line
				inArr = shuffleArray(inArr); // added line
			} // added line
			return inArr; // added line
		} // added line
	 	public static void printCardArr(ArrayList<Card> inArr) {
	 		for (int i=0; i < inArr.size(); i++) {
	 			System.out.print(inArr.get(i).name);


And we can test what we've done by adding this to the main method:

	 			cardArr.add(new Card( names[i], prices[i] ));
	 		}
	 		printCardArr(cardArr);
			cardArr = bogoSortValue(cardArr); // added line
			printCardArr(cardArr); // added line
	 	}
	}

Below the methods we just wrote, we can write the methods necessary for "bogosort by name." We can create a function that iterates over characters in the names, and compares them. Then, we can create a function that uses the previous function to check if an array is sorted. And then, we can create our `bogoSortName` function:

	 		}
	 		return inArr;
	 	}
		public static boolean isBeforeString(String stringOne, String stringTwo) { // added line
			char[] charArrOne = stringOne.toCharArray(); // added line
			char[] charArrTwo = stringTwo.toCharArray(); // added line
			int lesserLength = Math.min(charArrOne.length, charArrTwo.length); // added line
			for (int i=0; i<lesserLength; i++) { // added line
				if (charArrOne[i] < charArrTwo[i]) { // added line
					return true; // added line
				} else if (charArrOne[i] > charArrTwo[i]) { // added line
					return false; // added line
				} // added line
			} // added line
			return true; // added line
		} // added line
		public static boolean checkSortedName(ArrayList<Card> inArr) { // added line
			for (int i=0; i<inArr.size() - 1; i++) { // added line
				if (!isBeforeString(inArr.get(i).name, inArr.get(i+1).name)) { // added line
					return false; // added line
				} // added line
			} // added line
			return true; // added line
		} // added line
		public static ArrayList<Card> bogoSortName(ArrayList<Card> inArr) { // added line
			while (!checkSortedName(inArr)) { // added line
				inArr = shuffleArray(inArr); // added line
			} // added line
			return inArr; // added line
		} // added line
	 	public static void printCardArr(ArrayList<Card> inArr) {

And we can test our `bogoSortName` function:

    	 	printCardArr(cardArr);
	 		cardArr = bogoSortValue(cardArr);
	 		printCardArr(cardArr);
			cardArr = bogoSortName(cardArr); // added line
			printCardArr(cardArr); // added line
	 	}
	 }