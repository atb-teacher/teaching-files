# Contacts Lab

## Due: November 2nd

Make a REPL in Java that can load the provided dataset [data_to_parse.txt](data_to_parse.txt), and create an array of Contacts. You will be provided a text file full of names, ages, and addresses. The user should be able to sort the Contacts by name. They should choose whether they want to use Bogosort, Bubblesort, or Mergesort.

Once the project is finished, I will quiz you for a short time (2-5 minutes) on your own code, just to make sure you are making deliberate decisions.

If there is a part that many students struggle with, I will add an additional hint in the git repo.

## Stretch goals (+2% extra credit each)

1. Let the user save Contacts to a CSV

2. Let the user retrieve Contacts by name, age, or address

3. Let the user save Contacts to a database

4. Use Spring to render a "Contact" webpage

5. Let the user add a new contact
