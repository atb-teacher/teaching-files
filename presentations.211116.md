# Presentations from the Core Computer Science textbook

* Presentations will start on Tuesday, the 16th of November

* Presentations should be 15-20 minutes long

* Presentations should come with a worksheet, consisting of 7-10 questions

  * Rough drafts of the worksheets are due on Tuesday, the 9th of November

  * Consider using Google Forms for the worksheets


