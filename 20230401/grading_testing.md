# Grade Lab Unit Tests
## A Java Lab
## 10 points

## Part 1

I tried making a java program that takes in an integer, and provides a letter grade. That program is [here](Gradingv1.java). Using [JUnit's Assert methods](https://junit.org/junit4/javadoc/latest/org/junit/Assert.html), try to create tests that test Normal Data, Data at the Limits, Extreme Data, ~~and Abnormal Data~~(1) for my program.

It's okay if the testing method errors out because a test is failed.

Here is an example of a Main.java file in replit that tests for normal data.

```java
import org.junit.Assert;

public class Main {
  public static void main(String[] args) {
    Assert.assertEquals(
      Gradingv1.giveLetterGrade(40),
      'F'
    );
    System.out.println("Passed all tests!");
  }
}
```

Note: to compile this program, you can hit the big green "Run" button, or you can compile and run the code with the argument `-classpath .:target/dependency/*`. Otherwise java will not be able to find the import.

1) Abnormal Data can't be tested because Java has type checking.

## Part 2

Make a slightly improved version of my grading lab, called `Gradingv2.java`. The improved version should give a letter grade for all integers between 0 and 200. You should create a custom exception that extends the Exception class, and that exception should be thrown if an integer is provided that is out of range. Create a separate file that tests `Gradingv2`.
