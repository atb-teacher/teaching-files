# Steps to Land

path = "🌊🌊🌊🌊🚢🌊🌊🌊🌊🌊🌊🌊🌊🏝️🌊🌊🌊🌊🌊"

def check_new_distances(index, _distances, _highest):
    """Set new highest number of steps in the list of distances"""
    if index < len(_distances) - 1:
        if _distances[index + 1] is None:
            _distances[index + 1] = _highest + 1
    if index > 0:
        if _distances[index - 1] is None:
            _distances[index - 1] = _highest + 1
    print(_distances)
        

def find_distances(_path, _distances, _highest):
    """Find the distances from the ship to different points in the list"""
    for i, one_distance in enumerate(_distances):
        if one_distance == _highest: # if you are exploring the highest number of steps you've explored before
            check_new_distances(index=i, _distances=_distances, _highest=_highest)
    
    if None in _distances: # if you aren't done replacing all Nones with distances
        _highest += 1 
        find_distances(_path=_path, _distances=_distances, _highest=_highest) # rerun the same function with a higher limit of steps

def main(_path):
    distances = [None for one_char in _path] # Make a list of Nones that are the length of the list
    for i, one_char in enumerate(_path):
        if one_char == "🚢":
            distances[i] = 0 # the distance at the index of the ship is zero
            find_distances(_path=_path, _distances=distances, _highest=0) # find all the different positions' distances from zero
    for i, one_char in enumerate(_path):
        if one_char == "🏝️"[0]: # the [0] is because of weird unicode stuff
            print(f"Distance to {one_char} is {distances[i]}")
            

main(path)
