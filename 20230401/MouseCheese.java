import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/*
 * Write a method that starts at the mouse's position
 * and finds a path to the cheese. The method should be recursive.
 * In the map below,
 * # = wall
 * . = walkable floor
 * m = mouse
 * c = cheese
 */

public class MouseCheese {
	public static void main(String args[]) {
		Map mainMap = new Map(
"""
.........c
.####.####
......####
#####.####
####..####
####.m####
.###.#####
c....#####
###.######
###c######
"""
		);
		mainMap.print();
	}

}

class Map {
	public List<List<MapCell>> map = new ArrayList<List<MapCell>>(10);
	public Map(String strMap) {
		strMap = strMap.strip();
		String[] splitMap = strMap.split("\n");
		for (int i=0; i<splitMap.length; i++) {
			map.add(
				Arrays.asList(
						splitMap[i].split("")
				).stream()
				.map(str -> new MapCell(str))
				.collect(Collectors.toCollection(ArrayList::new))
			);
		}
	}
	public void print() {
		for (List<MapCell> row: map) {
			for (MapCell cell: row) {
				cell.print();
			}
			System.out.println();
		}
	}
}

class MapCell {
	public static enum CellType {WALL, FLOOR, CHEESE, START};
	public CellType type;
	public char repr;
	public MapCell(String mapChar) {
		repr = mapChar.charAt(0);
		switch (mapChar) {
			case "m":
				type = CellType.START;
				break;
			case "c":
				type = CellType.CHEESE;
				break;
			case ".":
				type = CellType.FLOOR;
				break;
			case "#":
				type = CellType.WALL;
				break;
		}
	}
	public void print() {
		System.out.print(repr);
	}
}
