class AMaker {
	public static String makeA(String strComponent, int counter) {
		counter--;
		if (counter <= 0) {
			return strComponent + "a";
		}
		else return makeA(strComponent + "a", counter);
	}
	public static String makeA(int counter) {
		return makeA("", counter);
	}

	public static void main(String[] args) {
		System.out.println(
			makeA(10)
		);
	}
}
