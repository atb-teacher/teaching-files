# Recursive Password Generator
## A Java Lab
## 5 points

Make a recursive method that will produce a password consisting of lowercase letters. You can simply choose randomly from an array of lowercase letters. There should be two methods with the same name -- one will take in a number, and one will take in both a number and a string to build off of.

> makePassword(5) // "xbrja"

> makePassword("abc", 2) // "abcrj"

[Hint file -- AMaker](AMaker.java)
