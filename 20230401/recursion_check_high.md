# Recursive Array Element Value Checker
## A Java Lab
## 5 points

Make a recursive method that will produce a password consisting of lowercase letters. You can simply choose randomly from an array of lowercase letters. There should be two methods with the same name -- one will take in a number, and one will take in both a number and a string to build off of.

Make a recursive method that checks through an array of integers and returns the first index that has a corresponding value that is smaller than the given lower limit.

```java
checkArray(
	{95, 80, 70, 50, 30, 0, 0}, // array of integers
	50 // limit to compare integers against
) // returns 4, which is the 1st index of a number smaller than 50
checkArray(
	{30, 29, 28, 27, 26, 25, 24}, // array of integers
	29 // limit to compare integers against
) // returns 2, which is the 1st index of a number smaller than 29
```
