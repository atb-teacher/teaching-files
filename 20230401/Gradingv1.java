public class Gradingv1 {
  public static char giveLetterGrade(int numGrade) {
    if (numGrade < 60) {
      return 'F';
    }
    else if (numGrade < 70) {
      return 'D';
    }
    else if (numGrade < 80) {
      return 'C';
    }
    else if (numGrade < 90) {
      return 'B';
    }
    else if (numGrade < 100) {
      return 'A';
    }
    else return '?';
  }
}
