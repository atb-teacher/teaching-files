# Ticket to Ride Lab
## A Java lab

You are given this game board ![game board](ticket-to-ride.png) as well as a csv file that has the cards required to connect each city [here](ticket-to-ride-distances.csv). The delimiter is not a comma `,` but rather it is a tilde `~`.

The game looks something like this:

```
(Denver) == (Kansas City)
   |
(Albuquerque)
```

In this example, connecting Denver and Kansas City requires four cards, and connecting Denver and Albuquerque requires one card. There is no direction: connecting Seattle and Helena is the same as connecting Helena and Seattle.

You will write a program that analyzes the csv file, lets the user choose two cities mentioned in the csv file, and figures out the minimum number of cards required to to make a train line from one city to another. For example, making a train line from Albuquerque to Kansas City, the shortest path would be through Denver. It would require five cards: one card to connect Albuquerque to Denver, and four cards to connect Denver to Kansas City. If the user chooses `Albuquerque` and `Kansas City`, the program should print out that the smallest number of cards to connect them is five.

This program is not required to be recursive.
