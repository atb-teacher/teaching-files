nums = [0, 5, -5, 3, 10]

def find_the_smallest(in_list):
    smallest = None
    for one_num in in_list:
        if smallest is None or smallest > one_num:
            smallest = one_num
    return smallest

print(find_the_smallest(nums))
