# Mouse and Cheese
## A group Java lab
### for 1-3 students

Start with this code: [MouseCheese.java](MouseCheese.java).

The given code generates a map that looks like this:

```
.........c
.####.####
......####
#####.####
####..####
####.m####
.###.#####
c....#####
###.######
###c######
```

Where `#` represents wall, `.` represents walkable area, `m` represents the mouse's starting point, and `c` represents a block of cheese. The `Map` class has a `map` attribute, which is a 2D array of `MapCell` objects, each one having a `CellType` (Enum) of `WALL`, `FLOOR`, `CHEESE`, OR `START`.

Create a recursive method that will find ~~the shortest path to each piece of cheese~~ the smallest number of steps it takes to reach each piece of cheese. The mouse can go up, down, left, right, and cannot go diagonally. The method should have a parameter that keeps track of the number of steps it takes to get to each position. For example, it'd be good to know: 

```
.........c
.####.####
......####
#####2####
####21####
####1m####
.###2#####
c....#####
###.######
###c######
```

It'd be good to know all the places that the mouse can go in one step, all the places the mouse can go in two steps, etc.

Here's a hint file: [steps\_to\_land.py](steps_to_land.py). The file uses a recursive algorithm to find the minimum number of steps to reach each position, and uses the resulting map to find the distance to land.
