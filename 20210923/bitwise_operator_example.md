Bitwise Operator Examples

The "and" operator looks like `&`. It receives two bits, and returns true if and only if both bits received were true. The "or" operator looks like `|`. It receives two bits, and returns false if and only if both bits received were false.

You can look at the image here:

![Bitwise Operator Examples](rect82.png)

You should be able to envision each column being evaluated with the `&` or `|` operator, and producing the correct result under the column.

    jshell> Integer.parseInt("11110000", 2)
    $1 ==> 240
    
    jshell> Integer.parseInt("10101010", 2)
    $2 ==> 170
    
    jshell> 240 & 170
    $3 ==> 160
    
    jshell> Integer.toBinaryString(160)
    $4 ==> "10100000"
    
    jshell> 240 | 170
    $5 ==> 250
    
    jshell> Integer.toBinaryString(250)
    $6 ==> "11111010"
