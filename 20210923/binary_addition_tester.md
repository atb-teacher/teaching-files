# Binary addition tester

Let's make a small program that tests the user's ability to add numbers in binary. We will write the program in python, but you should be able to translate the program to Java with a little help.

Generally games have loops, so we can start with a loop.

    while True: # added line
    	user_choice = input("(p)lay or (quit): ") # added line
    	if user_choice.lower() == "q": # added line
    		break # added line
    	elif user_choice.lower() != "p": # added line
    		continue # added line
    	print("*" * 100) # added line

First, we add a loop that will always run. Then, we let the user choose if they want the loop to continue. the `.lower()` method converts a string to lowercase, so if the user enters `Q`, that capital `Q` will be converted into a lowercase `q`, and the expression `"q" == "q"` will resolve to true and the loop will break. `break` stops the loop immediately and exits the loop, whereas `continue` stops the loop immediately and goes back to the top of the loop.

If we import the `random` module, we should be able to get random numbers to use. `randrange(n)` gives us a random number between `0` and `n`. It is lower-bound inclusive and upper-bound exclusive, which means that it can give us `0`, but it cannot give us `n`.

We're going to add an addition test to our code, but unfortunately we will make an error:

    import random # added line
    
     while True:
     	user_choice = input("(p)lay or (quit): ")
     	if user_choice.lower() == "q":
     		break
     	elif user_choice.lower() != "p":
     		continue
    	print("*" * 100) # deleted line
    	int1 = random.randrange(8) # added line
    	int2 = random.randrange(8) # added line
    	print(f"What is {int1} + {int2}?") # added line
    	user_guess = input(": ") # added line
    	if int1 + int2 == user_guess: # added line
    		print("Correct!") # added line

Try to get the math right. You should find that you're very bad at math, even if it gives you something simple like `2 + 2`. Our problem is in this line: `if int1 + int2 == user_guess:`. On the left side of the `==` operator we have an integer, and on the right side we have `user_guess`, which refers to a string. `input` always returns a string.

We can fix this error easily.

     	int2 = random.randrange(8)
     	print(f"What is {int1} + {int2}?")
     	user_guess = input(": ")
    	if int1 + int2 == user_guess: # deleted line
    	if int1 + int2 == int(user_guess): # added line
     		print("Correct!")

Now we can try to show the user the numbers in binary, instead of in base 10. Python has a built-in function, `bin`, that returns a binary string. The string should have a prefix, `0b`, to indicate that it's binary, followed by a series of ones and zeroes.

In this phase of the program, the user will still type in base 10.

     	int1 = random.randrange(8)
     	int2 = random.randrange(8)
    	print(f"What is {int1} + {int2}?") # deleted line
    	bin1 = bin(int1) # added line
    	bin2 = bin(int2) # added line
    	print(f"What is {bin1} + {bin2}?") # added line
     	user_guess = input(": ")
     	if int1 + int2 == int(user_guess):
     		print("Correct!")

This program should work, if you can just do the conversion between binary and base-10 in your head.

Python can convert from a binary string to an integer, and we will be doing all our math with integers (even though python is doing binary math under the hood). I guess python is invested in lying to us.

To convert from binary string to integer, we can use the `int` function. However, we have to let `int` know what type of number it is expecting. We will use the syntax `int(binary_string, 2)`. The `2` lets `int` know that we're converting from base-2 to base-10.

We can also make our binary numbers look cleaner by splicing the strings. In python, you can splice a string with the syntax `"abcdefg"[2:4]` to get `"cd"`. The `2` is the start of the splice, and the `4` is the end of the splice. It is lower-bound inclusive, and upper-bound exclusive, so we see the character at position `2`, but we don't see the character at position `4`.

If there is no upper bound, the string will go to the end. `"abcdefg"[2:]` gives us `"cdefg"`.

So we can hide the binary strings' prefix, `"0b"`, by splicing it out.

     	bin1 = bin(int1)
     	bin2 = bin(int2)
    	print(f"What is {bin1} + {bin2}?") # deleted line
    	print(f"What is {bin1[2:]} + {bin2[2:]}?") # added line
     	user_guess = input(": ")
    	if int1 + int2 == int(user_guess): # deleted line
    	user_guess_int = int(user_guess, 2) # added line
    	if int1 + int2 == user_guess_int: # added line
     		print("Correct!")

We can add a response if the user gets something wrong.

     	user_guess_int = int(user_guess, 2)
     	if int1 + int2 == user_guess_int:
     		print("Correct!")
    	else: # added line
    		correction = bin(int1 + int2) # added line
    		print(f"Incorrect. The correct answer was {correction}") # added line

Again, we're printing out the prefix of the binary string, so we can clean that up.

     	else:
     		correction = bin(int1 + int2)
    		print(f"Incorrect. The correct answer was {correction}") # deleted line
    		print(f"Incorrect. The correct answer was {correction[2:]}") # added line

Unfortunately, our program will error out if someone types a base-10 number or a letter. We can catch such errors with a `try except` statement. Python will "try" to do everything in the `try` block, and if an error arises it will run the code in the `except` block.

(In this step, there's a lot of "deleted" and "added" lines that are just indented one additional level).

     	print(f"What is {bin1[2:]} + {bin2[2:]}?")
     	user_guess = input(": ")
    	user_guess_int = int(user_guess, 2) # deleted line
    	if int1 + int2 == user_guess_int: # deleted line
    		print("Correct!") # deleted line
    	else: # deleted line
    		correction = bin(int1 + int2) # deleted line
    		print(f"Incorrect. The correct answer was {correction[2:]}") # deleted line
    	try: # added line
    		user_guess_int = int(user_guess, 2) # added line
    		if int1 + int2 == user_guess_int: # added line
    			print("Correct!") # added line
    		else: # added line
    			correction = bin(int1 + int2) # added line
    			print(f"Incorrect. The correct answer was {correction[2:]}") # added line
    	except: # added line
    		print("The guess must be a binary number.") # added line
    		continue # added line
     	
    
Unfortunately, we're following a poor coding pattern. It isn't good just to have `try`, it can catch too many errors. To demonstrate this problem, let's intentionally add an error into our `try` block:

     	print(f"What is {bin1[2:]} + {bin2[2:]}?")
     	user_guess = input(": ")
     	try:
    		5 + 'a' # added line
     		user_guess_int = int(user_guess, 2)
     		if int1 + int2 == user_guess_int:
     			print("Correct!")

Now, any input should trigger the alert, and tell the user that their input is not proper.

In this example, the error is easy to find because we intentionally put it there. However, if the error were something we overlooked, it would be maddening.

The error we are trying to catch is a `ValueError`.

    >>> int('a')
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    ValueError: invalid literal for int() with base 10: 'a'

So we can change our code like this:

     		else:
     			correction = bin(int1 + int2)
     			print(f"Incorrect. The correct answer was {correction[2:]}")
    	except: # deleted line
    	except ValueError: # added line
     		print("The guess must be a binary number.")
     		continue

Now our code won't work, but we'll know what's going on, which is far more important.

After testing it, we can remove our intentional error.

     	user_guess = input(": ")
     	try:
    		5 + 'a' # deleted line
     		user_guess_int = int(user_guess, 2)
     		if int1 + int2 == user_guess_int:
     			print("Correct!")

If you want to do a little bit more with this example, you can use `zfill` to make the binary numbers appear with a consistent number of digits.

    >>> my_str = "123"
    >>> my_str.zfill(8)
    '00000123'

