# HTML Notes part 4

Take notes on a section of the book, in an HTML file. Your notes should combine to be a minimum of 200 words, or 150 words if you have use a list element. Word count should not include html code.

Turn in a file with the given filename, or a link to a replit that has the given filename as a project name. In replit, we'll keep the main file named `index.html`. You should not work with other students who have your section (that will come later).

Here are the sections of the book you should take notes on:


```
Part 1
3.1.1 pg 126, 127
```

* Thalen, Tanush, Jacob

-----

```
Part 2
3.1.1 129 - 133
```

* Carter, Declan, Farzaan
-----

```
Part 3
3.1.1 134-136
```

* Reid, Isaac, Nathan
-----


```
Part 4
3.1.1 137-139
```

* Aila, Kyle, Tru
-----


```
Part 5
3.1.3
```

* Ethan, Siena, Dillon
-----


```
Part 6
3.1.4
```

* Nick, Matthew, Sofronis
-----


```
Part 7
3.1.5 - 3.1.8 (ignore example boxes)
```

* Braden, Kieran, Lucas
-----
