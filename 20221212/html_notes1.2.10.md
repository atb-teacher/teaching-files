# HTML Notes part 3

Take notes on a section of the book, in an HTML file. Your notes should combine to be a minimum of 200 words, or 150 words if you have use a list element. Word count should not include html code.

Turn in a file with the given filename, or a link to a replit that has the given filename as a project name. In replit, we'll keep the main file named `index.html`. You should not work with other students who have your section (that will come later).

Here are the sections of the book you should take notes on:


```
Part 1
1.2.12, 1.2.13
```

* Thalen, Tanush, Jacob

-----

```
Part 2
1.2.14
```

* Carter, Declan, Farzaan
-----

```
Part 3
1.2.15
```

* Reid, Isaac, Nathan
-----


```
Part 4
2.1.1
```

* Aila, Kyle, Tru
-----


```
Part 5
2.1.2, 2.1.3
```

* Ethan, Siena, Dillon
-----


```
Part 6
2.1.4, 2.1.5 through page 70
```

* Nick, Matthew, Sofronis
-----


```
Part 7
2.1.5 page 72-74, 2.1.6
```

* Braden, Kieran, Lucas
-----

