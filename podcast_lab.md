# Podcast Episode Viewer
### A Java Assignment

Create a program that takes an rss feed as an argument, and lists the first five episodes of the podcast. When the user chooses an episode, show more details for that episode.

```
java RssViewer "https://feeds.npr.org/510282/podcast.xml" # NPR's Pop Culture podcast
fetching episodes...

1) Russian Doll
2) Grace and Frankie
3) Ozark
4) Titanic
5) Summer Movie Guide

What would you like to know more about?
: 5

Summary: It's about to be summer. While that means sunshine and maybe even a chance to relax, it also means summer entertainment. Whether you're ready to venture out into theaters or you're still enjoying the comforts of your couch, we are here to talk about some of the best TV, film and music that's coming straight to you. Vote for your favorite American Idol contestants at npr.org/AmericanIdol
```

You can start with [this file](HttpTest.java), which runs on replit.com.

### Extra credit +1 point

Extra credit for moving all inner html tags, such as `<bold>`, `<span>`, `<em>`, etc.
