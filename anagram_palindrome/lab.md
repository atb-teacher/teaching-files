# Anagram and Palindrome lab

1) Create a REPL (read evaluate print loop, a while loop that lets the user add info)
2) Let the user choose anagram or palindrome
3) If "anagram", they can enter two words
4) If "palindrome", they can enter one word
5) Tell them if it is or isn't
note: the program should be able to handle commas and spaces

