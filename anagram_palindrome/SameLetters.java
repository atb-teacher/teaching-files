import java.util.Scanner;
import java.util.ArrayList;

/*
This program lets the user enter a bunch of letters twice, and checks if they did it correctly.
 */

public class SameLetters {

	public static void main(String[] args) {
		Scanner myObj = new Scanner(System.in);
		ArrayList<Character> charArr = new ArrayList<Character>();
		boolean breakLoop = false;
		String oneChar;
		while (!breakLoop) {
			System.out.println("Enter a character: ");
			oneChar = myObj.nextLine();
			if (oneChar.equals("done")) {
				breakLoop = true;
			}
			else {
				charArr.add(oneChar.toCharArray()[0]);
			}
		}
		breakLoop = false;
		int counter = 0;
		while ((!breakLoop) && (counter < charArr.size())) {
			System.out.print("Repeat the #");
			System.out.print(counter + 1);
			System.out.println("character you entered: ");
			oneChar = myObj.nextLine();
			if ((oneChar.toCharArray()[0]) == charArr.get(counter)) {
				System.out.println("Great!");
			}
			else {
				System.out.println("Miss.");
			}
			counter++;
		}

	}

}
