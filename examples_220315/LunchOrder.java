/*
 * Example file for testing
 */


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class LunchOrder {
	public static void main(String args[]) {
		assertEquals(1, 1); // nothing will happen
		assertEquals(1, 2); // error out

		LunchOrder l1 = new LunchOrder(2, "SANDWICH");
		LunchOrder l2 = new LunchOrder(2, "SANDWICH");
		// assertTrue(false);
		assertTrue(l1.checkEquals(l2));

		LunchOrder l3 = new LunchOrder(0, "SANDWICH");
		LunchOrder l4 = new LunchOrder(0, "SANDWICH");
		assertTrue(l3.checkEquals(l4));

		try {
			LunchOrder l5 = new LunchOrder(0, "sAnDwIcH");
			LunchOrder l6 = new LunchOrder(0, "sAnDwIcH");
			fail();
		}
		catch(java.lang.IllegalArgumentException e) {
			System.out.println("Sarcastic case doesn't work");

		}
		

		
		
	}
	int amount;
	enum Food {
		SANDWICH,
		BURRITO,
		RICE_BOWL,
	}
	Food food;

	LunchOrder(int foodAmount, String chosenFood) {
		this.amount = foodAmount;
		this.food = Food.valueOf(chosenFood);
	}

	public boolean checkEquals(LunchOrder otherLunch) {
		return (
			(this.amount == otherLunch.amount) && 
			(this.food == otherLunch.food)
		);
	}
}
