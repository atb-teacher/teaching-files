public class thisExample {
	public static void main(String[] args) {
		Tally myTally = new Tally()
			.addI()
			.addI()
			.addI()
			.addI();
		myTally.printTally();
	}
}

class Tally {
	String Is;
	Tally() {
		Is = "I";
	}
	public Tally addI() {
		this.Is = this.Is + "I";
		System.out.println(this);
		return this;
	}
	public void printTally() {
		System.out.println(Is);
	}
}
