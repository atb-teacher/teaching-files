/*
 * Example file for UML diagrams
 * Correspoding to the textbook, on page 299
 */
import java.util.ArrayList;
public class PetOwner {
	public static void main(String args[]) {
		PetOwner Pat = new PetOwner();
		Pat.getPet(new Dog());
		Pat.getPet(new Dog());
		Pat.getPet(new Cat());
		Pat.printPets();

	}
	
	ArrayList<Pet> pets;

	PetOwner()  {
		pets = new ArrayList<Pet>();
	}

	public void getPet(Pet newPet) {
		pets.add(newPet);
		newPet.owner = this;
	}

	public void printPets() {
		int petsLength = pets.size();
		for (int i=0; i<petsLength; i++) {
			System.out.println(pets.get(i).sound);
		}
	}
}

class Pet {
	public PetOwner owner;
	public String sound;
}

class Cat extends Pet {
	Cat() {
		sound = "meow";
	}
}

class Dog extends Pet {
	Dog() {
		sound = "woof";
	}
}
