public class SodaTypes3 {
	public static void main(String args[]) {
		System.out.println("Hello!");
		Soda newSoda = Soda.startOfChain().build();
		newSoda.printStuff();

	}
}
	
class Soda {
	String name;
	String flavor;
	boolean caffeinated;
	float sugar;

	public static SodaBuilder startOfChain() {
		return new SodaBuilder();
	}
	static class SodaBuilder {
		String name = "Sprite";
		String flavor = "lemony";
		boolean caffeinated = false;
		float sugar = 17;

		public Soda build() {
			return new Soda( this );
		}
	}

	Soda ( SodaBuilder builderParam ) {
		name = builderParam.name;
		flavor = builderParam.flavor;
		caffeinated = builderParam.caffeinated;
		sugar = builderParam.sugar;
	}


	public void printStuff() {
		System.out.print(" name ");
		System.out.println(name);
		System.out.print(" flavor ");
		System.out.println(flavor);
		System.out.print(" caffeinated ");
		System.out.println(caffeinated);
		System.out.print(" sugar ");
		System.out.println(sugar);
	}
	
}
