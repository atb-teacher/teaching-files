# Bubble Sort

In this section, we will write our own implementation of Bubble Sort. This one can be tough, so you can work with a partner, and don't be afraid to email me for help.

 Goal 1: Sort a list with one out-of-place element

Let's start by sorting lists that look like this:

    [0, 1, 100, 2, 3]

This is a list with one element out of place, and that element simply needs to be pushed to the right.

First, how do we find the out-of-place element? The out-of-place element should be the one that is bigger than the elemnt to the right. So we can look at every index, and look at every index to the right. This is a good opportunity for us to get an error we got in a previous walkthrough.

    >>> sample_list = list(range(10))
    >>> sample_list
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    >>> import random
    >>> sample_list.insert(random.randrange(10), 100)
    >>> sample_list
    [0, 1, 2, 100, 3, 4, 5, 6, 7, 8, 9]
    >>> for index in range(len(sample_list)):
    ...     print(index)
    ...
    0
    1
    2
    3
    4
    5
    6
    7
    8
    9
    10
    >>> for index in range(len(sample_list)):
    ...     print(sample_list[index])
    ...
    0
    1
    2
    100
    3
    4
    5
    6
    7
    8
    9
    >>> for index in range(len(sample_list)):
    ...     print(sample_list[index], sample_list[index + 1])
    ...
    0 1
    1 2
    2 100
    100 3
    3 4
    4 5
    5 6
    6 7
    7 8
    8 9
    Traceback (most recent call last):
      File "<stdin>", line 2, in <module>
    IndexError: list index out of range

Once again, if we want to look to the right, we need to make sure that we aren't trying to look right from the rightmost index. If you imagine the list, `['A', 'B', 'C',]`, you can look to the right of the `'A'`, but you can't look to the right of the `'C'`.

We will start out with a simple function that finds the index of the out-of-place number. The error above demonstrates why we need to use `len(in_list) - 1` to get every index except the last one. Using `len(in_list)` to get every index would result in an error.

	import random  added line
	
	def bubble_sort(in_list):  added line
		num_index = None  added line
		for i in range(len(in_list) - 1):  added line
			if in_list[i] > in_list[i + 1]:  added line
				num_index = i  added line
		print(f"The unsorted number is at index {num_index}")  added line
	
	sample_list = list(range(10))  added line
	sample_list.insert(random.randrange(10), 100)  added line
	
	print(sample_list)  added line
	bubble_sort(sample_list)  added line

Try running this file, it should point out where the `100` is.

Now, let's just try to push that number in the right direction.

	 	num_index = None  deleted line
	 	for i in range(len(in_list) - 1):
	 		if in_list[i] > in_list[i + 1]:
				num_index = i  deleted line
				in_list[i], in_list[i + 1] = in_list[i + 1], in_list[i]  added line
	 	print(f"The unsorted number is at index {num_index}")  deleted line

This bit of code, `in_list[i], in_list[i + 1] = in_list[i + 1], in_list[i]`, is used to swap two numbers in python. It's called `tuple packing/tuple unpacking`, meaning under the hood python is creating and using collections. Don't worry about learning it.

In Java you might make a temporary variable to swap two values. You can also swap values with `xor`, but I wouldn't recommend that for now.

Actually, we can make it easier to see what happens when our function runs:

	 	for i in range(len(in_list) - 1):
	 		if in_list[i] > in_list[i + 1]:
				print(f"The list is {in_list}")  added line
				print(f"Moving {in_list[i]}")  added line
	 			in_list[i], in_list[i + 1] = in_list[i + 1], in_list[i]

Now, if we run the program, we should see the number `100` step towards the right. The "bubble" nature of this algorithm is that the number `100` is simply looking to it's immediate right, and isn't looking anywhere outside of the "bubble." It's a lot of small stepping.

 Goal 2: Sort a list with two out-of-place elements

First, let's just add another out-of-place element to our sample list, and look at the output:

	sample_list = list(range(10))
	sample_list.insert(random.randrange(10), 100)
	sample_list.insert(random.randrange(10), 101)  added line
	 
	print(sample_list)
	bubble_sort(sample_list)

Here's the output I got:

    [0, 1, 100, 2, 3, 4, 5, 6, 101, 7, 8, 9]
    The list is [0, 1, 100, 2, 3, 4, 5, 6, 101, 7, 8, 9]
    Moving 100
    The list is [0, 1, 2, 100, 3, 4, 5, 6, 101, 7, 8, 9]
    Moving 100
    The list is [0, 1, 2, 3, 100, 4, 5, 6, 101, 7, 8, 9]
    Moving 100
    The list is [0, 1, 2, 3, 4, 100, 5, 6, 101, 7, 8, 9]
    Moving 100
    The list is [0, 1, 2, 3, 4, 5, 100, 6, 101, 7, 8, 9]
    Moving 100
    The list is [0, 1, 2, 3, 4, 5, 6, 100, 101, 7, 8, 9]
    Moving 101
    The list is [0, 1, 2, 3, 4, 5, 6, 100, 7, 101, 8, 9]
    Moving 101
    The list is [0, 1, 2, 3, 4, 5, 6, 100, 7, 8, 101, 9]
    Moving 101
    [0, 1, 2, 3, 4, 5, 6, 100, 7, 8, 9, 101]

We can see three basic phases here:
1. The algorithm notices that `100` is bigger than the number to the right, so it keeps moving `100` to the right.
2. The algorithm notices that `100` is not bigger than `101` once `101` is directly to the right of `100`, so it shifts its focus from `100` to `101`. It was looking at `100` for a while, simply because `100` kept on having a smaller number to the right of it, but once that was no longer true our function dropped the `100`.
3. The algorithm moves `101` to the right.

The 2nd phase is very short, but it's an important conceptual point: if our function is simply looking in a small bubble, and asking "Is the number 100, and then the number 101, sorted?", it will understand that small section as sorted. Our function is working well, because it does not push the `100` to the right of the `101`.

Let's try doing this in two stages (here it says "add" and "delete" a lot, but it's mostly indented lines):

	 def bubble_sort(in_list):
		for i in range(len(in_list) - 1):  deleted line
			if in_list[i] > in_list[i + 1]:  deleted line
				print(f"The list is {in_list}")  deleted line
				print(f"Moving {in_list[i]}")  deleted line
				in_list[i], in_list[i + 1] = in_list[i + 1], in_list[i]  deleted line
		for stage in ["Stage 1", "Stage 2"]:  added line
			print(stage)  added line
			for i in range(len(in_list) - 1):  added line
				if in_list[i] > in_list[i + 1]:  added line
					print(f"The list is {in_list}")  added line
					print(f"Moving {in_list[i]}")  added line
					in_list[i], in_list[i + 1] = in_list[i + 1], in_list[i]  added line

This seems to work. I will show to sample outputs. One where the `100` starts to the right of the `101`:

    [0, 1, 101, 2, 3, 4, 5, 100, 6, 7, 8, 9]
    Stage 1
    The list is [0, 1, 101, 2, 3, 4, 5, 100, 6, 7, 8, 9]
    Moving 101
    The list is [0, 1, 2, 101, 3, 4, 5, 100, 6, 7, 8, 9]
    Moving 101
    The list is [0, 1, 2, 3, 101, 4, 5, 100, 6, 7, 8, 9]
    Moving 101
    The list is [0, 1, 2, 3, 4, 101, 5, 100, 6, 7, 8, 9]
    Moving 101
    The list is [0, 1, 2, 3, 4, 5, 101, 100, 6, 7, 8, 9]
    Moving 101
    The list is [0, 1, 2, 3, 4, 5, 100, 101, 6, 7, 8, 9]
    Moving 101
    The list is [0, 1, 2, 3, 4, 5, 100, 6, 101, 7, 8, 9]
    Moving 101
    The list is [0, 1, 2, 3, 4, 5, 100, 6, 7, 101, 8, 9]
    Moving 101
    The list is [0, 1, 2, 3, 4, 5, 100, 6, 7, 8, 101, 9]
    Moving 101
    Stage 2
    The list is [0, 1, 2, 3, 4, 5, 100, 6, 7, 8, 9, 101]
    Moving 100
    The list is [0, 1, 2, 3, 4, 5, 6, 100, 7, 8, 9, 101]
    Moving 100
    The list is [0, 1, 2, 3, 4, 5, 6, 7, 100, 8, 9, 101]
    Moving 100
    The list is [0, 1, 2, 3, 4, 5, 6, 7, 8, 100, 9, 101]
    Moving 100
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 100, 101]


And one where the `100` starts to the left of the `101`:

    [0, 1, 100, 2, 3, 4, 5, 6, 101, 7, 8, 9]
    Stage 1
    The list is [0, 1, 100, 2, 3, 4, 5, 6, 101, 7, 8, 9]
    Moving 100
    The list is [0, 1, 2, 100, 3, 4, 5, 6, 101, 7, 8, 9]
    Moving 100
    The list is [0, 1, 2, 3, 100, 4, 5, 6, 101, 7, 8, 9]
    Moving 100
    The list is [0, 1, 2, 3, 4, 100, 5, 6, 101, 7, 8, 9]
    Moving 100
    The list is [0, 1, 2, 3, 4, 5, 100, 6, 101, 7, 8, 9]
    Moving 100
    The list is [0, 1, 2, 3, 4, 5, 6, 100, 101, 7, 8, 9]
    Moving 101
    The list is [0, 1, 2, 3, 4, 5, 6, 100, 7, 101, 8, 9]
    Moving 101
    The list is [0, 1, 2, 3, 4, 5, 6, 100, 7, 8, 101, 9]
    Moving 101
    Stage 2
    The list is [0, 1, 2, 3, 4, 5, 6, 100, 7, 8, 9, 101]
    Moving 100
    The list is [0, 1, 2, 3, 4, 5, 6, 7, 100, 8, 9, 101]
    Moving 100
    The list is [0, 1, 2, 3, 4, 5, 6, 7, 8, 100, 9, 101]
    Moving 100
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 100, 101]


When the `101` is to the left of the `100`, our function finds the `101` first, since our function looks from left-to-right. At no point will `101` find a larger number to the right, so our function will continue pushing `101` to the right. That is stage 1. Stage 2 is finding `100`, and pushing that to the right.

When the `100` is left of the `101`, our function will push `100` to the right, until it sees that the slice `[100, 101]` is sorted, and stops moving `100`. The first stage starts by moving `100` to the right, then shifts to moving `101` to the right. At the end of the first stage, `101` is sorted. Then, in stage 2, `100` is caught and moved to the right.

For the sake of clarity, we are going to try sorting a list with three unsorted elements, before we just create a generic Bubble Sort.

 Goal 3: Sort a list with three out-of-place element

Let's add our 3rd phase:

	def bubble_sort(in_list):
		for stage in ["Stage 1", "Stage 2"]:  deleted line
		for stage in ["Stage 1", "Stage 2", "Stage 3"]:  added line
	 		print(stage)
	 		for i in range(len(in_list) - 1):
	 			if in_list[i] > in_list[i + 1]:

And:

	sample_list = list(range(10))
	sample_list.insert(random.randrange(10), 100)
	sample_list.insert(random.randrange(10), 101)
	sample_list.insert(random.randrange(10), 102)  added line

Here's a sample output I got:

    [0, 1, 2, 101, 3, 4, 102, 100, 5, 6, 7, 8, 9]
    Stage 1
    The list is [0, 1, 2, 101, 3, 4, 102, 100, 5, 6, 7, 8, 9]
    Moving 101
    The list is [0, 1, 2, 3, 101, 4, 102, 100, 5, 6, 7, 8, 9]
    Moving 101
    The list is [0, 1, 2, 3, 4, 101, 102, 100, 5, 6, 7, 8, 9]
    Moving 102
    The list is [0, 1, 2, 3, 4, 101, 100, 102, 5, 6, 7, 8, 9]
    Moving 102
    The list is [0, 1, 2, 3, 4, 101, 100, 5, 102, 6, 7, 8, 9]
    Moving 102
    The list is [0, 1, 2, 3, 4, 101, 100, 5, 6, 102, 7, 8, 9]
    Moving 102
    The list is [0, 1, 2, 3, 4, 101, 100, 5, 6, 7, 102, 8, 9]
    Moving 102
    The list is [0, 1, 2, 3, 4, 101, 100, 5, 6, 7, 8, 102, 9]
    Moving 102
    Stage 2
    The list is [0, 1, 2, 3, 4, 101, 100, 5, 6, 7, 8, 9, 102]
    Moving 101
    The list is [0, 1, 2, 3, 4, 100, 101, 5, 6, 7, 8, 9, 102]
    Moving 101
    The list is [0, 1, 2, 3, 4, 100, 5, 101, 6, 7, 8, 9, 102]
    Moving 101
    The list is [0, 1, 2, 3, 4, 100, 5, 6, 101, 7, 8, 9, 102]
    Moving 101
    The list is [0, 1, 2, 3, 4, 100, 5, 6, 7, 101, 8, 9, 102]
    Moving 101
    The list is [0, 1, 2, 3, 4, 100, 5, 6, 7, 8, 101, 9, 102]
    Moving 101
    Stage 3
    The list is [0, 1, 2, 3, 4, 100, 5, 6, 7, 8, 9, 101, 102]
    Moving 100
    The list is [0, 1, 2, 3, 4, 5, 100, 6, 7, 8, 9, 101, 102]
    Moving 100
    The list is [0, 1, 2, 3, 4, 5, 6, 100, 7, 8, 9, 101, 102]
    Moving 100
    The list is [0, 1, 2, 3, 4, 5, 6, 7, 100, 8, 9, 101, 102]
    Moving 100
    The list is [0, 1, 2, 3, 4, 5, 6, 7, 8, 100, 9, 101, 102]
    Moving 100
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 100, 101, 102]

Here let's focus on the rightmost numbers. In stage 1, it's `9`, which is not the biggest number. In stage 2, the rightmost number is `102`, which is the largest number. In stage 3, the two rightmost numbers are `101` and `102`, the two largest numbers.

Basically, the more times we go through a phase of "move the largest number to the right," the part of the list that is sorted grows bigger, and the part of the list that is unsorted grows smaller. We can change our function accordingly.

It's important to remember that we are starting with a sorted list, which is not realistic. When we see the final list, `[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 100, 101, 102]`, the triple-digit numbers represent the numbers that our function sorted, and our function can have confidence that those numbers are sorted. The single-digit numbers represent the sorted numbers that our function was handed, and our function does not necessarily know that these numbers are sorted. So our function should be looking on the left side to see if there's more sorting to do.

 Goal 4: Sort an unsorted list

We will start by having a different stage for every number in the list:

    import random
	 
	def bubble_sort(in_list):
		for stage in ["Stage 1", "Stage 2", "Stage 3"]:  deleted line
			print(stage)  deleted line
		for stage_num in range(len(in_list)):  added line
			print(f"Stage {stage_num + 1}")  added line
			for i in range(len(in_list) - 1):
	 			if in_list[i] > in_list[i + 1]:
					print(f"The list is {in_list}")  deleted line
					print(f"Moving {in_list[i]}")  deleted line
					print(f"Moving index {i} to index {i+1}")  added line
	 				in_list[i], in_list[i + 1] = in_list[i + 1], in_list[i]
	 
	sample_list = list(range(10))  deleted line
	sample_list.insert(random.randrange(10), 100)  deleted line
	sample_list.insert(random.randrange(10), 101)  deleted line
	sample_list.insert(random.randrange(10), 102)  deleted line
	sample_list = list(range(100, 110))  added line
	random.shuffle(sample_list)  added line
	 
	print(sample_list)
	bubble_sort(sample_list)

Before we talk about the output of this program, I will explain a small test I ran:

    sample_list2 = list(range(105, 99, -1))
    print(sample_list2)
    bubble_sort(sample_list2)
    print(sample_list2)

The syntax of the `list(range())` is a little wonky:

	>>> list(range(105, 99, -1))
    [105, 104, 103, 102, 101, 100]

That's just creating a list that's counting down from `105` to `100`. I sorted this list with our function (you don't have to), and I got this output:

    [105, 104, 103, 102, 101, 100]
    Stage 1
    Moving index 0 to index 1
    Moving index 1 to index 2
    Moving index 2 to index 3
    Moving index 3 to index 4
    Moving index 4 to index 5
    Stage 2
    Moving index 0 to index 1
    Moving index 1 to index 2
    Moving index 2 to index 3
    Moving index 3 to index 4
    Stage 3
    Moving index 0 to index 1
    Moving index 1 to index 2
    Moving index 2 to index 3
    Stage 4
    Moving index 0 to index 1
    Moving index 1 to index 2
    Stage 5
    Moving index 0 to index 1
    Stage 6
    [100, 101, 102, 103, 104, 105]

Let me change the print in my program, so we can see which number is moving:

    [105, 104, 103, 102, 101, 100]
    Stage 1
    Moving number 105
    Moving number 105
    Moving number 105
    Moving number 105
    Moving number 105
    Stage 2
    Moving number 104
    Moving number 104
    Moving number 104
    Moving number 104
    Stage 3
    Moving number 103
    Moving number 103
    Moving number 103
    Stage 4
    Moving number 102
    Moving number 102
    Stage 5
    Moving number 101
    Stage 6
    [100, 101, 102, 103, 104, 105]

In order to sort all 6 numbers, we never actually need to move the smallest number, so we only need 5 stages to move everything around the number `100`. So we can remove a stage.

	 def bubble_sort(in_list):
		for stage_num in range(len(in_list)):  deleted line
		for stage_num in range(len(in_list) - 1):  added line
	 		print(f"Stage {stage_num + 1}")
	 		for i in range(len(in_list) - 1):
	 			if in_list[i] > in_list[i + 1]:

Now, let's look at the program's output. Here's an example of output I got:

    [109, 104, 108, 100, 106, 103, 101, 102, 107, 105]
    Stage 1
    Moving index 0 to index 1
    Moving index 1 to index 2
    Moving index 2 to index 3
    Moving index 3 to index 4
    Moving index 4 to index 5
    Moving index 5 to index 6
    Moving index 6 to index 7
    Moving index 7 to index 8
    Moving index 8 to index 9
    Stage 2
    Moving index 1 to index 2
    Moving index 2 to index 3
    Moving index 3 to index 4
    Moving index 4 to index 5
    Moving index 5 to index 6
    Moving index 6 to index 7
    Moving index 7 to index 8
    Stage 3
    Moving index 0 to index 1
    Moving index 2 to index 3
    Moving index 3 to index 4
    Moving index 4 to index 5
    Moving index 6 to index 7
    Stage 4
    Moving index 1 to index 2
    Moving index 2 to index 3
    Moving index 3 to index 4
    Moving index 5 to index 6
    Stage 5
    Moving index 1 to index 2
    Moving index 2 to index 3
    Stage 6
    Stage 7
    Stage 8
    Stage 9
    [100, 101, 102, 103, 104, 105, 106, 107, 108, 109]


There's two things to notice here: First, in the early stages, the rightmost index we look at is 8/9. That number goes down with every stage. Our function is still checking the higher indices, so it's inefficient in that way.

Second, stages 6-10 didn't need to move anything. In the sample above, if we look at the numbers `100`, `101`, and `102`, those three numbers are sorted relative to each other. Once we grab all the big numbers and shift them right, those three leftover numbers will be naturally sorted because we got lucky. It is also likely that we don't need all our stages because our function will push locally-large numbers to the right before finding the largest number and pushing it to the right. So if our list starts with `[101, 100]`, our function will push the `101` to the right before eventually focusing on `109` in stage 1 (assuming that `109` doesn't begin in the rightmost position).

We can start ignoring the right numbers in later stages, which means that we ignore the higher indices:

	 	for stage_num in range(len(in_list) - 1):
	 		print(f"Stage {stage_num + 1}")
			for i in range(len(in_list) - 1):  deleted line
			for i in range(len(in_list) - 1 - stage_num):  added line
	 			if in_list[i] > in_list[i + 1]:
	 				print(f"Moving index {i} to index {i+1}")
	 				in_list[i], in_list[i + 1] = in_list[i + 1], in_list[i]

We can also add a flag to make sure that there is a part of the list that's unsorted. If we run through the part of the list that is potentially still unsorted, and we don't have to change anything, then we should just stop our algorithm from running another stage.

	 	for stage_num in range(len(in_list) - 1):
	 		print(f"Stage {stage_num + 1}")
			found_unsorted = False  added line
	 		for i in range(len(in_list) - 1 - stage_num):
	 			if in_list[i] > in_list[i + 1]:
					found_unsorted = True  added line
	 				print(f"Moving index {i} to index {i+1}")
	 				in_list[i], in_list[i + 1] = in_list[i + 1], in_list[i]
			if not found_unsorted:  added line
				break  added line

And now we have written a Bubble Sort algorithm!

 Lab 1 (required): Rewrite this algorithm in Java

 Lab 2 (not required) (advanced): With the bogosort lab, let the user choose if they want to sort contacts by bogosort or by bubblesort
